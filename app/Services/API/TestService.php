<?php


namespace App\Services\API;


use App\Exceptions\NotEnoughQuestions;
use App\Jobs\NotifyUsersToNewTest;
use App\Models\Test;
use App\QueryFilter\Tests\FilterIfPruchesed;
use Illuminate\Pipeline\Pipeline;
use App\Services\AbstractService;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;
use App\QueryFilter\Tests\FilterByUserId;
use \Illuminate\Contracts\Pagination\LengthAwarePaginator;

class TestService extends AbstractService
{

    /**
     * @return LengthAwarePaginator
     */
    public function paginate(): LengthAwarePaginator
    {

        $test = app(Pipeline::class)
            ->send(Test::query())
            ->through([
                FilterByUserId::class
            ])
            ->thenReturn();

        $test
            ->select(DB::raw("*,(select sum(test_rate.rate)/count(test_rate.id) from test_rate where test_rate.test_id=tests.id) as totalRate"))
            ->AprovedAndPublished()
            ->with(['level', 'category', 'user'])
            ->withCount('sessions')
            ->orderBy('totalRate', 'DESC')
            ->orderBy('sessions_count', 'DESC');


        return QueryBuilder::for($test)->allowedFilters([
            AllowedFilter::exact('level'),
            AllowedFilter::exact('category'),
        ])->paginate(50)->appends(request()->query());

    }

    /**
     * @param $test_id
     * @return mixed
     */
    public function getOne($test_id)
    {
        $test = app(Pipeline::class)
            ->send(Test::query())
            ->through([
                FilterIfPruchesed::class
            ])
            ->thenReturn();

        return $test
            ->where("id", $test_id)
            ->AprovedAndPublished()
            ->with(['level', 'category', 'user','comments.user','comments.comments.user'])
            ->withCount('questions')
            ->withCount('sessions')
            ->first();

    }

    /**
     * @param $test_id
     */
    public function show($test_id)
    {

    }

    /**
     * @param $test
     * @param $request
     * @return mixed
     */
    public function update($test, $request)
    {
        $data = $request
            ->only(['category', 'level', 'title', 'description', 'price', 'second_per_question', 'questions_per_session', 'video']);
        return $test->update($data);
    }

    /**
     * @param $input
     * @param $user
     * @return Test[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function store($input, $user)
    {
        //todo request filter
        $input['video'] = $input['video'] ? $this->convertYoutubeVideoToEmbed($input['video']) : null;
        $input['user_id'] = $user->id;
        $input['price'] = (float)$input['price'];
        $test = Test::create($input);
        return Test::where('id', $test->id)->with('category')->with('level')->get();
    }

    /**
     * @param $youtubeVideo
     * @return false|string
     */
    private function convertYoutubeVideoToEmbed($youtubeVideo)
    {
        $str = $youtubeVideo;
        $start = strpos($str, 'v=');
        $link = substr($str, $start + 2);

        if (strpos($str, '&')) {
            $retrevedLink = strrev($link);
            $cleanRetrevedLink = substr($retrevedLink, strpos($retrevedLink, '&') + 1);
            $link = strrev($cleanRetrevedLink);
        }

        return $link;
    }

    /**
     * @param $test
     * @param $user
     * @return bool
     * @throws NotEnoughQuestions
     */

    public function publishTest($test, $user): bool
    {

        if ($test->published) {
            return true;
        }

        if ($test->questions()->count() <= 0) {
            throw new NotEnoughQuestions("can't publish with 0 questions");
        }

        if ($test->questions_per_session && $test->questions()->count() < $test->questions_per_session) {
            throw new NotEnoughQuestions("the limit is $test->questions_per_session question for this test");
        }

        $test->published = true;
        $test->save();
        //todo handle emails
        NotifyUsersToNewTest::dispatch($test)->delay(Carbon::now()->addMinutes(2));

        return true;
    }


}
