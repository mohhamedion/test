<?php


namespace App\Services\API;

use App\Exceptions\CreateModelException;
use App\Exceptions\NotEnoughCoins;
use App\Models\WithdrawalRequest;
use App\Services\AbstractService;
use App\Models\Withdrawal;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class WithdrawalService extends AbstractService
{
    private float $minimal_amount_to_withdrawal = 10;//in $

    /**
     * @param $data
     * @return mixed
     */
    public function store($data)
    {
        //todo validation job
        return Withdrawal::create($data);
    }

    public function paginate($user)
    {
        return withdrawal::where('user_id', $user->id)
            ->orderBy('created_at', 'DESC')
            ->paginate();
    }

    /**
     * @param $user
     * @return bool
     * @throws CreateModelException
     * @throws NotEnoughCoins
     */

    public function sendWithdrawalRequest($user): bool
    {
        if (!$user->wallet->coins) {
            throw new NotEnoughCoins("لا يوجد لديك رصيد ");
        }

        if ($user->wallet->coins < $this->minimal_amount_to_withdrawal) {
            throw new NotEnoughCoins("الحد الادنى للسحب $" . $this->minimal_amount_to_withdrawal);
        }

        $amount = $user->wallet->coins;
        $amount = (float)number_format($amount, 2);

        try {

            DB::transaction(function() use ($user,$amount){

                WithDrawalRequest::create([
                    'user_id' => $user->id,
                    'amount' => $amount,
                    'approved_by' => Auth::user()->id,
                    'status' => 'PENDING'
                ]);


                $user->wallet->coins -= $amount;
                $user->wallet->frozen_coins += $amount;
                $user->wallet->save();

            });

        } catch (CreateModelException $ex) {
            throw new CreateModelException($ex->getMessage());
        }

        return true;
    }
}
