<?php

namespace App\Services\API;

use App\Notifications\NewPurchase;
use App\Services\AbstractService;
use App\Models\Purchase;

class PurchasesService extends AbstractService
{
    /**
     * @param $user
     * @return Purchase|\Illuminate\Database\Eloquent\Builder
     */
    public function get($user)
    {
        return Purchase::where('seller_id', $user->id);
    }

    /**
     * @param $user
     * @param $purchasesable
     * @param $cost
     * @return bool
     */
    public function purchase($user, $purchasesable, $cost): bool
    {
        //todo change test_id to referenced_id
        //todo test for ownership

//        $user->purchases()->create([
//            'cost' => $cost,
//            'purchasesable_id' => $purchasesable->id,
//            'seller_id' => $purchasesable->user_id
//        ]);

        $purchasesable->purchases()->create([
            'cost' => $cost,
            'seller_id' => $purchasesable->user_id,
            'buyer_id'=>$user->id
        ]);

        $purchasesable->user->wallet->coins += (float)number_format($cost,2);
        $purchasesable->user->wallet->save();
        $purchasesable->user->notify(new NewPurchase());

        return true;
    }

}
