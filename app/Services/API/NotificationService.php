<?php


namespace App\Services\API;


use App\Services\AbstractService;
use Illuminate\Support\Carbon;

class NotificationService extends AbstractService
{

    public function paginate($user)
    {
        $this->read($user);
       return  $user->notifications()->paginate(5);
    }

    public function countUnread($user){
        return $user->unreadNotifications->count();
    }

    public function read($user): void
    {
         $user->unreadNotifications()->update(['read_at' => Carbon::now()]);
    }

}
