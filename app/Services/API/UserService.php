<?php


namespace App\Services\API;

use App\Models\User;
use App\Services\AbstractService;
use App\Models\Withdrawal;

class UserService extends AbstractService
{

    /**
     * @param $input
     * @param $user
     */
    public function update($input,$user){
        $user->update($input);
    }


    /**
     * @param $usersIds
     * @return mixed
     */
    public function getUsersByIds($usersIds){
        //todo exept me
        return User::whereIn('id',$usersIds)->get();
    }

}
