<?php


namespace App\Services\API;

use App\Events\NewPurchese;
use App\Exceptions\CreateModelException;
use App\Exceptions\NoMoreAttemptsExeption;
use App\Exceptions\NotEnoughCoins;
use App\Models\Test;
use App\Models\TestRate;
use App\Models\TestSession;
use App\Notifications\NewPurchase;
use App\QueryFilter\TestSessions\FilterByUserId;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pipeline\Pipeline;
use App\Services\AbstractService;
use Illuminate\Support\Facades\DB;

class TestSessionService extends AbstractService
{

    /**
     * @return mixed
     */
    public function paginate()
    {
        $builder = app(Pipeline::class)
            ->send(TestSession::query())->through([
                FilterByUserId::class
            ])->thenReturn();

        return $builder->select(DB::Raw("*,  (select sum(mark)  from session_questions where session_questions.session_id=test_sessions.id ) as totalPoints"))
            ->orderBy('created_at', 'DESC')
            ->whereHas('test', function ($q) {
                $q->where('published', true);
            })
            ->with('test.category')
            ->withCount('sessionQuestions')
            ->paginate();

    }

    /**
     * @param $user
     * @return mixed
     */
    public function getCurrentSession($user)
    {
        //todo move auth logic to pipline

        return TestSession::where('user_id', $user->id)
            ->onGoing()
            ->with(['sessionQuestions.question.answers', 'test.category'])
            ->limit(1)
            ->get()
            ->last();

    }

    /**
     * @param $user
     * @return int
     */
    public function currentSessionExist($user): int
    {
        return TestSession::where('user_id', $user->id)->onGoing()->count();
    }

    /**
     * @param $session_id
     * @return mixed
     */
    public function getTestSessionResult($session_id)
    {
        //todo scope where
        $sessionResult = TestSession::where("id", $session_id)->where(function ($q) {
            $q->orWhere('going', false)->orWhere('end_at', '<=', Carbon::now());
        })
            ->with('sessionQuestions.sessionAnswers')
            ->with('sessionQuestions.question.answers')
            ->with('test.category')->with('test.level')->with('mark')
            ->get()->last();
        $user = request()->user('api');

        if ($user && $user->id === $sessionResult->user_id) {
            $sessionResult['showRate'] = (TestRate::where('user_id', $user->id)
                ->where('test_id', $sessionResult->test_id)
                ->count()) ? false : true;
        }

        return $sessionResult;
    }

    /**
     * @param $test_id
     * @return TestSession|Model
     * @throws CreateModelException
     */
    public function store($test_id)
    {
        $test = Test::findOrFail($test_id);
        $user = request()->user('api');
        $this->cancleAllSession($user->id);

        //todo move logic to another place
//        if ($user->id !== $test->user_id && $test->price && !$test->purchasesByUser()->count()) {
//            throw new NotEnoughCoins('هذا الاختبار غير مجاني');
//        }

        if ($this->outOfAttempts($test, $user->id)) {
            throw new CreateModelException("sorry, you are out of attempts");
        }

        $start_at = Carbon::now();
        $ends_at = Carbon::now();
        $questions_quantity = $test->questions_per_session > 0 ? $test->questions_per_session : $test->questions()->count();
        $ends_at->addSeconds($questions_quantity * $test->second_per_question);

        return TestSession::create([
            "test_id" => $test->id,
            "start_at" => $start_at,
            'user_id' => $user->id,
            "end_at" => $ends_at,
            "going" => true,
        ]);


    }

    /**
     * @param $user_id
     */
    private function cancleAllSession($user_id): void
    {
        TestSession::where('going', true)->where('user_id', $user_id)->update(['going' => false]);
    }

    /**
     * @param $test_id
     * @param $user_id
     * @return int
     */
    private function getNumberOfAttempts($test_id, $user_id): int
    {
        return TestSession::where('test_id', $test_id)->where('user_id', $user_id)->count();
    }

    /**
     * @param $test
     * @param $user_id
     * @return bool
     */
    private function outOfAttempts($test, $user_id)
    {
        $userAttempts = $this->getNumberOfAttempts($test->id, $user_id);
        return $test->number_of_attempts && $test->number_of_attempts <= $userAttempts;
    }


    public function testSessionsBestThisMonth($test_id)
    {
        return TestSession::where('test_id', $test_id)
            ->with('user')
            ->whereMonth("created_at", Carbon::now()->month)
            ->where(function ($q) {
                $q->orWhere("going", 0)->orWhere('end_at', '<', Carbon::now());
            })
            ->select(DB::Raw(" *,
     (select sum(mark) from session_questions where session_questions.session_id=test_sessions.id ) as totalPoints
     ,
     (updated_at-created_at) as inTime
     "))->orderBy('totalPoints', 'DESC')->groupBy('test_sessions.user_id')->withCount('sessionQuestions')->limit(3)->get();

    }

    public function getLastSessions($test_id)
    {
        return TestSession::where('test_id', $test_id)
            ->with('user')
            ->where(function ($q) {
                $q->orWhere("going", 0)->orWhere('end_at', '<', Carbon::now());
            })->select(DB::Raw("*,
    (select sum(mark)  from session_questions where session_questions.session_id=test_sessions.id ) as totalPoints"))
            ->withCount('sessionQuestions')
            ->limit(4)->orderBy('created_at', 'DESC')->get();

    }

}
