<?php

namespace App\Services\API;

use App\Exceptions\NotEnoughCoins;
use App\Exceptions\PaypalIssue;
use App\Jobs\CheckPaymentStatusJob;
use App\Models\PaymentGate;
use App\Models\Settings;
use App\Models\Test;
use App\Services\AbstractService;
use Carbon\Carbon;
use HttpException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use PayPalCheckoutSdk\Core\PayPalHttpClient;
use PayPalCheckoutSdk\Core\ProductionEnvironment;
use PayPalCheckoutSdk\Core\SandboxEnvironment;
use PayPalCheckoutSdk\Orders\OrdersCreateRequest;
use PayPalCheckoutSdk\Orders\OrdersCaptureRequest;
use PaypalPayoutsSDK\Payouts\PayoutsGetRequest;
use PaypalPayoutsSDK\Payouts\PayoutsPostRequest;
use Throwable;

class PaypalService extends AbstractService
{
    private PayPalHttpClient $client;
    private DepositService $depositService;
    private WithdrawalService $withdrawalService;
    private PurchasesService $purchasesService;
    private int $website_fee = 8;//percent
    private int $withdrawal_fee = 2;//percent


    public function __construct()
    {
        $this->depositService = new DepositService();
        $this->withdrawalService = new WithdrawalService();
        $this->purchasesService = new PurchasesService();
        $paymentStatus = Settings::select('payment_status')->first();
        $payment_gate = PaymentGate::where('gate', 'paypal')->first();
        $settings = json_decode($payment_gate->data);
        $sandbox = $paymentStatus->payment_status==="sandbox";

        if($sandbox){
            $clientId = $settings->sandbox->client_id;
            $clientSecret = $settings->sandbox->client_secret;
            $environment = new SandboxEnvironment($clientId, $clientSecret);

        }else{
            $clientId = $settings->production->client_id;
            $clientSecret = $settings->production->client_secret;
            $environment = new ProductionEnvironment($clientId, $clientSecret);
        }

        $this->client = new PayPalHttpClient($environment);
    }

    /**
     * @param $test_id
     * @return array|string
     */
    public function createPayment(Test $test)
    {
        $request = new OrdersCreateRequest();
        $request->prefer('return=representation');

        $request->body = [
            "intent" => "CAPTURE",
            "purchase_units" => [[
                "reference_id" => $test->id,
                "amount" => [
                    "value" => $test->price,
                    "currency_code" => "USD"
                ]
            ]],
            "application_context" => [
                "cancel_url" => "http://localhost:8000/paypal/cancle",
                "return_url" => "http://localhost:8000/paypal/thankyou"
            ]
        ];

        return $this->client->execute($request)->result;
    }


    /**
     * @param $orderID
     * @param $user
     * @return bool
     * @throws PaypalIssue
     * @throws \App\Exceptions\CreateModelException
     */
    public function executePayment($orderID, $user): bool
    {
        $res = new OrdersCaptureRequest($orderID);
        $res->prefer('return=representation');

        try {

            $response = $this->client->execute($res);
            $deposit = $this->depositService->store($response, $user->id);
            $test = Test::findOrFail($deposit->reference_id);
            //fee calculation
            $cost = (float)$deposit->amount - (float)$deposit->fee;
            $cost -= ($cost * ($this->website_fee + $this->withdrawal_fee) / 100);

            if ($deposit->status === 'COMPLETED') {
                $this->purchasesService->purchase($user, $test, $cost);
            }


        } catch (HttpException $ex) {
            throw new PaypalIssue('مشكلة في بايبال');
        }
        return true;
    }





    /**
     * @param $user
     * @throws NotEnoughCoins
     * @throws PaypalIssue
     * @throws \JsonException
     */
    public function sendPayout($withDrawalRequest)
    {

        $withDrawalRequest->load(['user']);
        $user= $withDrawalRequest->user;
        $email = $user->wallet->paypal_email ?: $user->email;
        $amount = $withDrawalRequest->amount;

        if($amount > $user->wallet->frozen_coins){
            throw new NotEnoughCoins('لا يوجد رصيد للسحب');
        }

        $request = new PayoutsPostRequest();
        $body = json_decode('{
                "sender_batch_header":
                {
                  "email_subject": "SDK payouts test txn"
                },
                "items": [
                {
                  "recipient_type": "EMAIL",
                  "receiver": "' . $email . '",
                  "note": "Your 1$ payout",
                  "sender_item_id": "1",
                  "amount":
                  {
                    "currency": "USD",
                    "value": ' . $amount . '
                  }
                }]
              }', true, 512, JSON_THROW_ON_ERROR);

        $request->body = $body;

        try {
            $response = $this->client->execute($request);
            $data = [
                'status' => $response->result->batch_header->batch_status,
                'batch_id' => $response->result->batch_header->payout_batch_id,
                'user_id' => $user->id,
                'amount' => $amount
            ];
            $withdrawal = $this->withdrawalService->store($data);

            $withDrawalRequest->withdrawal_id  = $withdrawal->id;
            $withDrawalRequest->status  = "AWATING";
            $withDrawalRequest->save();
            //todo transition  status using spatie

            CheckPaymentStatusJob::dispatch($withdrawal, $user)
                ->delay(Carbon::now()->addMinutes(1));

        } catch (throwable $exception) {
            throw new PaypalIssue($exception->getMessage(), 0, $exception);
        }
    }

    /**
     * @param $batch_id
     * @return mixed
     */
    public function getPaymentStatus($batch_id)
    {
        $request = new PayoutsGetRequest($batch_id);
        $response = $this->client->execute($request);
        return $response->result->batch_header->batch_status;
    }
}
