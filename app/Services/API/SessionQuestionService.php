<?php


namespace App\Services\API;


use App\Exceptions\CreateQuestionSessionError;
use App\Models\SessionQuestion;

use App\Services\AbstractService;

class SessionQuestionService extends AbstractService
{
    public function getOne($sessionQuestionId)
    {
        //todo checkIfSessionQuestionBelongsToSessionTest
        return SessionQuestion::find($sessionQuestionId);
    }

    public function store($question_id, $session)
    {
        //todo exeption
        try {
            SessionQuestion::create([
                "question_id" => $question_id,
                "session_id" => $session->id
            ]);
        } catch (CreateQuestionSessionError $error) {
            throw new CreateQuestionSessionError("حدث خطأ ما", $error);
        }

    }

    public function createSessionQuestions($session)
    {
        $test = $session->test;
        $testQuestions = $test->questions->toArray();

        $indexs =
            $test->questions_per_session ?
                array_rand($testQuestions, $test->questions_per_session) :
                array_keys($testQuestions);

        foreach ($indexs as $index) {
            $this->store($testQuestions[$index]['id'], $session);
        }
    }


}
