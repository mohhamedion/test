<?php


namespace App\Services\API;


use App\Models\TestRate;
use App\Services\AbstractService;

class RateTestService extends AbstractService
{

    public function store($input,$user)
    {

        $rate = $input;
        $rate['user_id'] = $user->id;
        $rate = TestRate::create($rate);
        return $rate;
    }

    public function show($test_id,$user_id){

        return TestRate::where('user_id', $user_id)
            ->where("test_id", $test_id)
            ->count();
    }
}
