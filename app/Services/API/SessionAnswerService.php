<?php


namespace App\Services\API;


use App\Models\SessionAnswer;
use App\Services\AbstractService;
use App\Exceptions\CreateAnswerSessionError;

class SessionAnswerService extends AbstractService
{
    public function store($answerId,$session){

        try{
        SessionAnswer::create([
            'session_question_id' => request()->session_question_id,
            'answer_id' => $answerId,
            'session_id' => $session->id,
            'choosen' => true
        ]);
        }catch(CreateAnswerSessionError $error){
            throw new CreateAnswerSessionError('حدث خطأ ما');
        }

    }

    public function createSessionAnswers($session) : bool {
        foreach (request()->answer_ids as $answerId):
            $this->store($answerId,$session);
        endforeach;

        return true;
    }
}
