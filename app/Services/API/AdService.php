<?php

namespace App\Services\API;

use App\Services\AbstractService;
use App\Models\Ad;
class  AdService extends AbstractService
{
    /**
     * @return mixed
     */
    public function paginate(){
        return Ad::paginate();
    }

}
