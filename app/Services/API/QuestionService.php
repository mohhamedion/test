<?php


namespace App\Services\API;


use App\Exceptions\CorrectAnswerDontExist;
use App\Models\Answer;
use App\Models\Question;
use App\Models\Test;
use Illuminate\Support\Facades\Auth;
use App\Services\AbstractService;
use App\Exceptions\CantDeleteQuestionException;

class QuestionService extends AbstractService
{

    public function store($input)
    {

        $user = Auth::user('api');
        $test = Test::findOrFail($input['test_id'])->where('user_id', $user->id);
        //todo check if test exist

        if ($this->ifCorrectAnswersDontExist($input['answers'])) {
            throw new CorrectAnswerDontExist('اجابة صحيحة واحد على الاقل');
        }
        $question = $input;
        $question['user_id'] = $user->id;
        $question = Question::create($question);
        $question->answers()->createMany($input['answers']);
        return Question::where('id', $question->id)->with('answers')->get();

    }

    public function update($input, $question_id)
    {
        if ($this->ifCorrectAnswersDontExist($input['answers'])) {
            throw new CorrectAnswerDontExist('اجابة صحيحة واحد على الاقل');
        }

        $question = Question::findOrFail($question_id);
        $question->update($input);
        $this->updateOrCreateAnswers($input['answers'], $question_id);
        $question = Question::where('id', $question_id)->with('answers')->get()->last();
        return $question;
    }


    public function delete($question_id)
    {
        $question = Question::findOrFail($question_id);
        $test = $question->test;
        $user = request()->user();

        if ($test->user_id == $user->id && $test->published == 0) {
            $question->answers()->delete();
            $question->delete();
            return true;
        }

        throw new CantDeleteQuestionException("لا تستطيع حذف اسألة اختبار منشور");

    }





    private function updateOrCreateAnswers($answers, $question_id)
    {//todo move to answersService
        foreach ($answers as $answer) {
            if (isset($answer['created_at'])) {
                 Answer::where('id', $answer['id'])->update([
                    'answer' => $answer['answer'],
                    'correct' => ($answer['correct'] == true),
                ]);
            } else {
                $answer['question_id'] = $question_id;
                Answer::create($answer);
            }
        }
    }

    private function ifCorrectAnswersDontExist($answers)
    {
        $notExist = true;
        foreach ($answers as $answer) {
            if ($answer['correct']) {
                $notExist = false;
            }
        }
        return $notExist;
    }


}
