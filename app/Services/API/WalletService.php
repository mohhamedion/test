<?php


namespace App\Services\API;

use App\Services\AbstractService;
use App\Models\Wallet;
use http\Exception;

class WalletService extends AbstractService
{
    public function getOne($id)
    {
        return Wallet::find($id);
    }

    public function update($request, $user)
    {

        $user->wallet->update(['paypal_email'=>$request->paypal_email]);

    }


}
