<?php


namespace App\Services\API;


use App\Jobs\NotifyTaggedUsers;
use App\Notifications\NewComment;
use App\Models\Comment;
use App\Models\Test;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Services\AbstractService;

class CommentService extends AbstractService
{

    /**
     * @param $input
     * @return mixed
     */
    public function store($input)
    {
        $user = Auth::user('api');
        $comment = $input;
        $comment['user_id'] = $user->id;
        $comment = Comment::create($comment);
        $comment['user'] = request()->user();
        $test = Test::find($input['test_id']);
        $testOwner = $test->user;
        //todo move to event
        if ($comment['comment_id']) {
            $userService = app()->make(UserService::class);
            $notifyUsers = $userService->getUsersByIds($input['tagging']);
            NotifyTaggedUsers::dispatch($notifyUsers, $comment)->delay(Carbon::now()->addMinutes(2));
        }

        if ($user->id !== $testOwner->id) {
            $testOwner->notify(new NewComment($test));
        }

        return $comment;
    }


    public function userOwnThisTest($testOwner, $commentOwner): bool
    {
        if ($testOwner !== $commentOwner) {
            return true;
        }
        return false;
    }

}
