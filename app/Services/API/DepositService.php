<?php


namespace App\Services\API;


use App\Exceptions\CreateModelException;
use App\Models\Deposit;
use App\Services\AbstractService;
use Throwable;

class DepositService extends AbstractService
{
    public function paginate($user)
    {
        return Deposit::where('user_id', $user->id)->orderBy('created_at', 'DESC')->paginate();
    }


    public function store($request, $user_id): Deposit
    {
        $result = $request->result;
        $payment = $result->purchase_units[0]->payments->captures[0];
        $payer = $result->payer;
        $data = [
            "user_id" => $user_id,
            'amount' => $payment->amount->value,
            'reference_id' => $result->purchase_units[0]->reference_id,
            'fee' => $payment->seller_receivable_breakdown->paypal_fee->value,
            'payment_id' => $payment->id,
            'payer' => $payer->name->given_name,
            'payer_email' => $payer->email_address,
            'payer_id' => $payer->payer_id,
            'status' => $result->status,
            'response' => json_encode($result)
        ];
        try {
            return Deposit::create($data);
        } catch (Throwable $ex) {
            throw new CreateModelException('خطأ في السيرفر',$ex->getMessage());
        }
    }

}
