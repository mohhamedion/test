<?php

namespace App\Jobs;

use App\Notifications\UserTaggedYou;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class NotifyTaggedUsers implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $users;
    private $comment;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($users,$comment)
    {
        $this->users = $users;
        $this->comment = $comment;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        foreach ($this->users as $user){
            $user->notify(new UserTaggedYou($this->comment->test , $this->comment->user));
        }
    }
}
