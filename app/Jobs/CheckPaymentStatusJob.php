<?php

namespace App\Jobs;

use App\Models\Withdrawal;
use App\Notifications\YourWithdrawalRequestAccepted;
use App\Services\API\PaypalService;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CheckPaymentStatusJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $user;
    private $withdrawal;
    private $paypalService;

    /**
     * Create a new job instance.
     *
     * @param $withdrawal
     * @param $user
     */
    public function __construct(WithDrawal $withdrawal, $user)
    {
        $this->user = $user;
        $this->withdrawal = $withdrawal;
        $this->paypalService = new PaypalService();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        $withdrawal = $this->withdrawal;

        $withdrawalRequest = $withdrawal->withdrawalRequest;
        $user = $this->user;
        $status = $this->paypalService->getPaymentStatus($withdrawal->batch_id);
        $status = str_replace('"', '', $status);

        $withdrawal->status = $status;
        $withdrawalRequest->status=$status;

        if ($status === "PENDING") {
            $this::dispatch($withdrawal,$user)->delay(Carbon::now()->addMinutes(1));
            return;
        }

        if ($status === "SUCCESS") {
            $user->wallet->frozen_coins -= $withdrawal->amount;
            $user->notify(new YourWithdrawalRequestAccepted());
        }



        if ($status === "DENIED") {
            $user->wallet->frozen_coins -= $withdrawal->amount;
            $user->wallet->coins += $withdrawal->amount;
        }

        $withdrawal->save();
        $withdrawalRequest->save();
        $user->wallet->save();
        //todo send notification
    }
}
