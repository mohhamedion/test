<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GetUsersWithIntress implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $category;

    /**
     * GetUsersWithIntress constructor.
     * @param $category
     */
    public function __construct($category)
    {
        //
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        return User::whereHas('testSessions', function ($query) {
            $query->whereHas('test', function ($query) {
                $query->where('category', $this->category->id);
            });
        })->get();
    }
}
