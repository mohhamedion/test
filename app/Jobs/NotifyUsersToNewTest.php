<?php

namespace App\Jobs;

use App\Models\User;
use App\Notifications\NewTest;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class NotifyUsersToNewTest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $test;
     /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($test)
    {
        $this->test= $test;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $test = $this->test;
        $category = $test->category()->get()->last();
        $users = GetUsersWithIntress::dispatchNow($category);

        foreach ($users as $user){
            $user->notify(new NewTest($test,$category));
        }

    }
}
