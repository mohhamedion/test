<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Level;
use \Illuminate\Http\JsonResponse;

class LevelController extends Controller
{
    //
    public function index(): JsonResponse
    {
        return response()->json(Level::get());
    }


}




