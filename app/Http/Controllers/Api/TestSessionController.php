<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\NotEnoughCoins;
use App\Http\Controllers\Controller;
use App\Notifications\NewPurchase;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;
use Hash;
use Auth;
use Carbon\Carbon;
use App\Http\Traits\SignMarksForQuestions;
use App\Http\Requests\TestSessions\StoreAnswerRequest;
use App\Http\Requests\TestSessions\StoreTestSessionRequest;
use App\Services\API\TestSessionService;
use App\Services\API\SessionQuestionService;
use App\Services\API\SessionAnswerService;
use \Illuminate\Http\JsonResponse;

class TestSessionController extends Controller
{
    //
    use SignMarksForQuestions;

    public float $price = 0;

    private TestSessionService $testSessionService;
    private SessionQuestionService $sessionQuestionService;
    private SessionAnswerService $sessionAnswerService;

    public function __construct
    (
        TestSessionService $testSessionService,
        SessionQuestionService $sessionQuestionService,
        SessionAnswerService $sessionAnswerService
    )
    {
        $this->testSessionService = $testSessionService;
        $this->sessionQuestionService = $sessionQuestionService;
        $this->sessionAnswerService = $sessionAnswerService;
    }

    public function paginate(): JsonResponse
    {
        $testSessions = $this->testSessionService->paginate();
        return response()->json($testSessions);
    }

    public function getTestSessionResult($session_id): JsonResponse
    {
        $sessionResult = $this->testSessionService->getTestSessionResult($session_id);
        return response()->json($sessionResult);
    }

    /**
     * @param StoreAnswerRequest $request
     * @return Application|ResponseFactory|Response
     */
    public function answerQuestion(StoreAnswerRequest $request)
    {
        $session = $this->testSessionService->getCurrentSession(Auth::user('api'));
        $sessionQuestion = $this->sessionQuestionService->getOne($request->session_question_id);

        if ($this->sessionAnswerService->createSessionAnswers($session)) {
            $sessionQuestion->answered = true;
            $sessionQuestion->save();
            $this->signMarksForQuestions($sessionQuestion, $request->answer_ids);
        }

        return response(null, 200);

    }

    /**
     * @return Application|ResponseFactory|JsonResponse|Response
     */
    public function show()
    {
        $testSession = $this->testSessionService->getCurrentSession(Auth::user('api'));

        if ($testSession) {
            extract($this->countLeftTimeForSession($testSession));
            return response()->json(["session" => $testSession, 'timeString' => $timeString, 'timeInteger' => $timeInteger]);
        }
        return response('status', 404);
    }

    /**
     * @return JsonResponse
     */
    public function currentSessionExist(): JsonResponse
    {
        $testSession = $this->testSessionService->currentSessionExist(Auth::user('api'));
        return response()->json($testSession);
    }

    /**
     * @param $testSession
     * @return array
     */
    private function countLeftTimeForSession($testSession): array
    {
        $endTime = new Carbon($testSession->end_at);
        $timer = $endTime->diffInSeconds(Carbon::now());
        $timeInteger = $timer;
        $timeString = gmdate('H:i:s', $timer);

        return ['timeString' => $timeString, 'timeInteger' => $timeInteger];
    }

    /**
     * @param StoreTestSessionRequest $request
     * @return Application|ResponseFactory|Response
     * @throws NotEnoughCoins
     */
    public function store(StoreTestSessionRequest $request)
    {
        $session = $this->testSessionService->store($request->post('test_id'));
        $this->sessionQuestionService->createSessionQuestions($session);
        return response('status', 200);

    }


    /**
     * @param $test_id
     * @return JsonResponse
     */
    public function bestUsersForTestSession($test_id): JsonResponse
    {
        $bestIn30Days = $this->testSessionService->testSessionsBestThisMonth($test_id);
        return response()->json($bestIn30Days);
    }

    /**
     * @param $test_id
     * @return JsonResponse
     */
    public function getLastSessionByTest($test_id): JsonResponse
    {
        $lastSessions = $this->testSessionService->getLastSessions($test_id);
        return response()->json($lastSessions);

    }
}




