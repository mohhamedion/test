<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\API\NotificationService;
use \Illuminate\Http\JsonResponse;

class NotificationController extends Controller
{
    //

    private NotificationService $notificationService;

    /**
     * NotificationController constructor.
     * @param NotificationService $notificationService
     */
    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    /**
     * @return JsonResponse
     */
    public function paginate(): JsonResponse
    {
        $result = $this->notificationService->paginate(auth()->user('api'));
        return response()->json($result);
    }

    /**
     * @return JsonResponse
     */
    public function countUnread(): JsonResponse
    {
        $result = $this->notificationService->countUnread(auth()->user('api'));
        return response()->json($result);
    }
}
