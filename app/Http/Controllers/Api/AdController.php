<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\API\AdService;
use Illuminate\Http\Request;
use \Illuminate\Http\JsonResponse;
class AdController extends Controller
{

    private AdService $adService;

    /**
     * AdController constructor.
     * @param AdService $adService
     */
    public function __construct(AdService $adService){
        $this->adService = $adService;
    }


    /**
     * @return JsonResponse
     */
    public function paginate(): JsonResponse
    {
        $result = $this->adService->paginate();
        return response()->json($result);
    }

}
