<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Validator;
use Hash;
use Auth;
use App\Models\SessionQuestion;
use App\Http\Traits\SignMarksForQuestions;
use App\Http\Requests\Questions\StoreQuestionRequest;
use App\Http\Requests\Questions\UpdateQuestionRequest;
use App\Services\API\QuestionService;
use \Illuminate\Http\JsonResponse;

class QuestionController extends Controller
{
    //
    use SignMarksForQuestions;

    private QuestionService $questionService;

    public function __construct(QuestionService $questionService)
    {
        $this->questionService = $questionService;
    }

    public function store(StoreQuestionRequest $request): JsonResponse
    {
        $lastQuestion = $this->questionService->store($request->post());
        return response()->json($lastQuestion);
    }

    public function update(UpdateQuestionRequest $request, $question_id): JsonResponse
    {
        $question = $this->questionService->update($request->post(), $question_id);
        return response()->json($question);

    }


    private function reSignAllSessionAnswersMarks($question_id)
    {
        $sessionQuestions = SessionQuestion::where('question_id', $question_id)->with('sessionAnswers')->get();
        foreach ($sessionQuestions as $sessionQuestion) {

            $answerIds = $this->objectToIndexArray($sessionQuestion->sessionAnswers);
            if (count($answerIds) > 0) {
                $this->SignMarksForQuestions($sessionQuestion, $answerIds);
            }
        }
        return true;
    }

    private function objectToIndexArray($assArray): array
    {
        $result = [];
        foreach ($assArray as $array) {
            $result[] = $array->answer_id;
        }
        return $result;
    }


    public function deleteQuestion($question_id)
    {
        $this->questionService->delete($question_id);
         return response(null, 204);

    }


}




