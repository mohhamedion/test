<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\NotAuthorized;
use App\Http\Controllers\Controller;
use App\Models\Test;
use App\Services\API\PaypalService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\JsonResponse;

class PaypalController extends Controller
{
    /**
     * @var PaypalService
     */
    private PaypalService $paypalService;

    /**
     * PaypalController constructor.
     * @param PaypalService $paypalService
     */
    public function __construct(PaypalService $paypalService)
    {
        $this->paypalService = $paypalService;

    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws NotAuthorized
     */
    public function createPayment(Request $request): JsonResponse
    {
        $test = Test::findOrFail($request->test_id);
        //todo use policy
        if ($request->user()->id === $test->user_id) {
            throw new NotAuthorized('لا تستطيع الشراء من نفسك');
        }

        $result = $this->paypalService->createPayment($test);
        return response()->json($result);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \App\Exceptions\CreateModelException
     * @throws \App\Exceptions\PaypalIssue
     */
    public function executePayment(Request $request): JsonResponse
    {
        $result = $this->paypalService->executePayment($request->orderID, Auth::user('api'));
        return response()->json($result);
    }

}
