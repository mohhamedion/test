<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\API\DepositService;
use \Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class DepositController extends Controller
{
    /**
     * @var DepositService
     */
    private DepositService $depositService;

    /**
     * DepositController constructor.
     * @param DepositService $depositService
     */
    public function __construct(DepositService $depositService)
    {
        $this->depositService = $depositService;
    }

    /**
     * @return JsonResponse
     */
    public function paginate(): JsonResponse
    {
        $response = $this->depositService->paginate(Auth::user('api'));
        return response()->json($response);
    }

}
