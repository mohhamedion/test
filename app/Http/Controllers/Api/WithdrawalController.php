<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\API\PaypalService;
use App\Services\API\WithdrawalService;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Http\JsonResponse;

class WithdrawalController extends Controller
{
    private WithdrawalService $withdrawalService;
    /**
     * @var PaypalService
     */
    private $paypalService;

    /**
     * WithdrawalController constructor.
     * @param WithdrawalService $withdrawalService
     * @param PaypalService $paypalService
     */
    public function __construct(WithdrawalService $withdrawalService, PaypalService $paypalService)
    {
        $this->withdrawalService = $withdrawalService;
        $this->paypalService = $paypalService;
    }

    /**
     * @return JsonResponse
     */
    public function paginate(): JsonResponse
    {
        $response = $this->withdrawalService->paginate(Auth::user('api'));
        return response()->json($response);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \App\Exceptions\CreateModelException
     * @throws \App\Exceptions\NotEnoughCoins
     */
    public function sendWithdrawalRequest()
    {
        $user = Auth::user('api');
        $this->withdrawalService->sendWithdrawalRequest($user);
        return response(null,204);
    }
}


