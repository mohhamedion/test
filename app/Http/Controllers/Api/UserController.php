<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\users\UpdateUserRequest;
use App\Services\API\TestService;
use App\Http\Controllers\Controller;
use App\Services\API\UserService;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\User;
use App\Http\Requests\Users\StoreUserRequest;
use App\Http\Requests\Users\LoginUserRequest;
use \Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Password;

class UserController extends Controller
{
    //
    private TestService $testService;
    private UserService $userService;

    public function __construct(TestService $testService, UserService $userService)
    {
        $this->testService = $testService;
        $this->userService = $userService;
    }

    /**
     * @param StoreUserRequest $request
     * @return JsonResponse
     */
    public function register(StoreUserRequest $request): JsonResponse
    {
        $user['email'] = $request->email;
        $user['firstname'] = $request->firstname;
        $user['lastname'] = $request->lastname;
        $user['password'] = Hash::make($request->password);
        $user['coin'] = 0;
        $user = User::create($user);
        $user->wallet()->create(['coins' => 0, 'paypal_email' => $user->email]);
        $token = $user->createToken('ProgHunt')->accessToken;
        return response()->json(['token' => $token, 'user' => $user]);
    }

    /**
     * @param LoginUserRequest $request
     * @return JsonResponse
     */
    public function login(LoginUserRequest $request): JsonResponse
    {
        $user = User::where('email', $request->email)->first();
        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                $token = $user->createToken('ProgHunt')->accessToken;
                if (!$user->wallet) {
                    $user->wallet()->create(['coins' => 0, 'paypal_email' => $user->email]);
                }
                return response()->json(['token' => $token, 'user' => $user]);
            }
        }
        return response()->json(['error' => ['email' => ['incorrect data']]], 401);
    }

    /**
     * @param $user_id
     * @return JsonResponse
     */
    public function show($user_id): JsonResponse
    {
        $user = User::where('id', $user_id)->get()->last();
        return response()->json($user);
    }

    /**
     * @param UpdateUserRequest $request
     * @return JsonResponse
     */
    public function update(UpdateUserRequest $request): JsonResponse
    {
        $user = Auth::user('api');
        $this->userService->update($request->post(), $user);
        return response()->json(null, 204);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function forgotPassword(Request $request): \Illuminate\Http\RedirectResponse
    {

        $request->validate(['email' => 'required|email']);

        $status = Password::sendResetLink(
            $request->only('email')
        );

        return $status === Password::RESET_LINK_SENT
            ? back()->with(['status' => __($status)])
            : back()->withErrors(['email' => __($status)]);

    }


    /**
     * @param Request $request
     * @return string
     */
    public function resetPassword(Request $request): string
    {

        $request->validate([
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:8|confirmed',
        ]);

        $status = Password::reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function ($user, $password) use ($request) {
                $user->forceFill([
                    'password' => Hash::make($password)
                ])->save();

                $user->setRememberToken(Str::random(60));

                event(new PasswordReset($user));
            }
        );

        return  Password::PASSWORD_RESET;

    }


}




