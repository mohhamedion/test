<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Tests\StoreTestRequest;
use App\Http\Resources\TestResource;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;
use App\Models\Test;
use App\Models\Question;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewTest;
use App\Services\API\TestService;
use \Illuminate\Http\JsonResponse;

class TestController extends Controller
{
    //

    protected int $TestSessionsCountLimitForRecomendation = 2;
    /**
     * @var TestService
     */
    private $testService;

    public function __construct(TestService $testService)
    {
        $this->testService = $testService;
    }

    /**
     * @return JsonResponse
     */

    public function index(): JsonResponse
    {
        $result = $this->testService->paginate();
        return response()->json($result);
    }

    /**
     * @param $test_id
     * @return TestResource
     */
    public function show($test_id): TestResource
    {
        $test = $this->testService->getOne($test_id);
        return new TestResource($test);
    }


    /**
     * @param StoreTestRequest $request
     * @return JsonResponse
     */
    public function store(StoreTestRequest $request): JsonResponse
    {
        $lastTest = $this->testService->store($request->post(), Auth::user('api'));
        return response()->json($lastTest);
    }

    /**
     * @param Request $request
     * @param Test $test
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request, Test $test): JsonResponse
    {
        $this->authorize('update', $test);
        $this->testService->update($test, $request);
        return response()->json(null, 204);
    }


    /**
     * @return JsonResponse
     */
    public function getBestByTestId(): JsonResponse
    {
        $data['Java'] = $this->getBestIn30ByCondition('Java', 5);
        return response()->json($data);
    }

    /**
     * @param $where
     * @param int $limit
     * @return mixed
     */
    private function getBestIn30ByCondition($where, $limit = 3)
    {
        return User::whereHas('testSessions', function ($q) use ($where) {
            $q->whereHas('test', function ($q) use ($where) {
                $q->whereHas('category', function ($q) use ($where) {
                    $q->where('category', $where);
                });
            });
        })
            ->select(DB::Raw(' *,
        (select sum(mark) from session_questions,test_sessions where session_questions.session_id = test_sessions.id and test_sessions.user_id=users.id)
        as totalPoints '))->limit($limit)->orderBy('totalPoints', 'DESC')->get();
    }


//

    /**
     * @return JsonResponse
     */
    public function getDraftTests(): JsonResponse
    {
        $draftTests = Test::where("user_id", Auth::user('api')->id)
            ->where('published', 0)
            ->with('category')
            ->with('level')
            ->get();
        return response()->json($draftTests);
    }

    /**
     * @param $test_id
     * @return JsonResponse
     */
    public function getDraftTestsById($test_id): JsonResponse
    {
        $test = Test::where("user_id", Auth::user('api')->id)
            ->where("id", $test_id)
            ->where('published', 0)
            ->with(['level', 'category', 'questions.answers'])
            ->get();
        return response()->json($test);
    }


    /**
     * @param $test_id
     * @return JsonResponse
     */
    public function getTestsById($test_id): JsonResponse
    {
        $test = Test::where("id", $test_id)
            ->AllowedByUser()
            ->with(['level', 'category', 'questions.answers'])
            ->get();
        return response()->json($test);
    }


    public function deleteDraftTest($test_id)
    {
        $test = Test::where('id', $test_id)->where('published', 0)->where('user_id', Auth::user('api')->id);
        if ($test) {
            $this->deleteQuestionsAndAnswersByTestId($test_id);
            $test->delete();
            return response(200);
        }
        return response(500);
    }

    /**
     * @param $test_id
     * @throws Exception
     */
    private function deleteQuestionsAndAnswersByTestId($test_id): void
    {
        $questions = Question::where('test_id', $test_id)->get();
        foreach ($questions as $question) {
            $question->answers()->delete();
            $question->delete();
        }
    }

//


    public function publishTest(Test $test)
    {
        $this->authorize('publish', $test);
        $this->testService->publishTest($test, Auth::user('api'));
        return response(null, 204);
    }


    private function sendNewTestEmailForIntrestedUsers($test_id, $category)
    {
        $users = $this->getPeopleWithIntress($category);
        $data['test_id'] = $test_id;
        $data['category'] = $category;
        $data['users'] = $users;

        foreach ($users as $user) {
            $data['username'] = $user->firstname;
            Mail::to($user->email)->send(new NewTest($data));
        }
    }

    /**
     * @param $category
     * @return array
     */
    private function getPeopleWithIntress($category): array
    {
        $arrayOfUsers = DB::select(DB::raw('select email,firstname,
                    (select count(*) from test_sessions
                    where users.id = test_sessions.user_id  and
                    EXISTS (select * from `tests` where `test_sessions`.`test_id` = `tests`.`id` and `category` = ?) ) as total
                    from users'), [$category]);

        $intrestedUsers = array_filter($arrayOfUsers, function ($user) {
            return $user->total >= $this->TestSessionsCountLimitForRecomendation;
        });
        return $intrestedUsers;
    }

    /**
     * @return JsonResponse
     */
    public function getBestFive(): JsonResponse
    {
        $bestfive = Test::select(DB::raw("*,(select sum(test_rate.rate)/count('test_rate.id') from test_rate where test_rate.test_id=tests.id) as totalRate"))
            ->AprovedAndPublished()
            ->with('level')
            ->with('user')
            ->with('category')->limit(4)->orderBy('totalRate', 'DESC')
            ->get();
        return response()->json($bestfive);
    }


}




