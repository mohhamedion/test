<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\wallets\UpdateWalletRequest;
use App\Services\API\WalletService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use  \Illuminate\Http\JsonResponse;

class WalletController extends Controller
{
    private WalletService $walletService;

    public function __construct(WalletService $walletService)
    {
        $this->walletService = $walletService;
    }

    public function update(UpdateWalletRequest $request): JsonResponse
    {
        $this->walletService->update($request, Auth::user('api'));
        return response()->json(null, 204);
    }

    public function showByUser(Request $request)
    {
        return $request->user()->wallet;
    }

}
