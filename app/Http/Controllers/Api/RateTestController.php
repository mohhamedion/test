<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\API\RateTestService;
use Illuminate\Http\JsonResponse;
use Validator;
use Hash;
use Auth;
use App\Http\Requests\Rates\StoreRateRequest;

class RateTestController extends Controller
{
    private RateTestService $rateTestService;

    public function __construct(RateTestService $rateTestService)
    {
        $this->rateTestService = $rateTestService;
    }

    /**
     * @param StoreRateRequest $request
     * @return JsonResponse
     */
    public function store(StoreRateRequest $request)
    {
        $user = Auth::user('api');
        $existRate =  $this->rateTestService->show($request->test_id,$user->id);
        if ($existRate > 0) {
            return response()->json(['error' => true], 500);
        }
        $rate = $this->rateTestService->store($request->post(),$user);
        if (!$rate) {
            return response()->json(['error' => true], 500);
        }
        return response()->json(200);

    }


}




