<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\API\PurchasesService;


class PurchasesController extends Controller
{
    /**
     * @var PurchasesService
     */
    private PurchasesService $purchasesService;

    /**
     * PurchasesController constructor.
     * @param PurchasesService $purchasesService
     */
    public function __construct(PurchasesService $purchasesService)
    {
        $this->purchasesService = $purchasesService;
    }

}
