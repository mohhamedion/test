<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Models\WebsiteMessage;

class WebsiteMessageController extends Controller
{
    /**
     * @param Request $request
     * @return bool|JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'text' => 'required|max:1000',
            'name' => 'required|max:255',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        WebsiteMessage::create($request->post());
        return true;


    }

}


