<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Models\WithdrawalRequest;
use App\Services\API\PaypalService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WithdrawalRequestController extends Controller
{
    private $paypalService;

    public function __construct(PaypalService $paypalService)
    {
        $this->paypalService = $paypalService;
    }

    public function index()
    {
        $table = WithdrawalRequest::with('user')->where('status','!=','SUCCESS')->get();

        return view('panel/withdrawalRequest/index', compact('table'));
    }

    public function sendPayout($withdrawalRequestId)
    {
        if(Auth::user()->role!=='admin'){
            abort(500);
        }
        $withdrawalRequest = WithdrawalRequest::where('status', 'PENDING')
            ->where('id', $withdrawalRequestId)
            ->where('withdrawal_id',null)
            ->first();
        if(!$withdrawalRequest){
            abort(500);
        }


        $this->paypalService->sendPayout($withdrawalRequest);

        return redirect()->back();
    }


}
