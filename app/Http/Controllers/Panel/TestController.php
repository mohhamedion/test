<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Test;

class TestController extends Controller
{
    public function index()
    {
        return Test::with('theCategory')->withCount('sessions')->with('user')->withCount('rates')->paginate();
//        return view('panel.tests.index', compact('tests'));
    }

    public function toggleApprove(Request $request)
    {
        $test = Test::where('id', $request->test_id)->get()->last();
        $test->approved = !$test->approved;
        $test->save();
        return response(null, 200);
//        return redirect()->back();
    }
}
