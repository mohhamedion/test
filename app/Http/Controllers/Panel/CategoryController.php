<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Http\Requests\Categories\StoreCategoryRequest;
use App\Http\Requests\Categories\UpdateCategoryRequest;
use App\Models\Category;

class CategoryController extends Controller
{
    public function index()
    {
        return Category::get();
    }

    public function store(StoreCategoryRequest $request)
    {
       return Category::create($request->validated());
    }

    public function update(UpdateCategoryRequest $request,Category $category): Category
    {
        $category->update($request->validated());
        return $category;
    }
}
