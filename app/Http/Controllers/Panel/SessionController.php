<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\TestSession;
use Illuminate\Support\Facades\Auth;

class SessionController extends Controller
{


    public function index()
    {
       return TestSession::orderBy('id', 'DESC')->with('user')->with('test')->paginate();
//        return view('panel.sessions.index', compact('sessions'));
    }


}
