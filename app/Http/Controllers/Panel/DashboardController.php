<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use App\Models\Category;
use Auth;
use App\Models\User;
use App\Models\Comment;
use App\Models\TestSession;
use App\Models\Settings;
use App\Models\Test;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;


class DashboardController extends Controller
{
    //
    public $namespace = 'panel';

    /**
     *
     */
    public function index()
    {
        // DB::Raw('SELECT created_at,count(*) from users GROUP BY created_at');
        $users = User::select(DB::raw('count(*) as total,date(created_at) as date '))->groupBy('date')->get();
        $sessions = TestSession::select(DB::raw('count(*) as total ,date(created_at) as date'))->groupBy('date')->get();
        $usersCount = User::count();
        $sessionsCount = TestSession::count();
        $TestsCount = Test::count();
        $CommentCount = Comment::count();
        $usersCountToday = User::whereDate('created_at', Carbon::today())->count();
        $SessionsCountToday = TestSession::whereDate('created_at', Carbon::today())->count();
        $TestsCountToday = Test::whereDate('created_at', Carbon::today())->count();
        $CommentCountToday = Comment::whereDate('created_at', Carbon::today())->count();

//        return view($this->namespace . '.dashboard.index', compact('usersCount',
//            'SessionsCount', 'TestsCount', 'CommentCount',
//            'usersCountToday',
//            'SessionsCountToday',
//            'TestsCountToday',
//            'CommentCountToday',
//            'users',
//            'sessions',
//        ));

        return [
            'users' => $users,
            'sessions' => $sessions,
            'usersCount' => $usersCount,
            'sessionsCount' => $sessionsCount,
            'testsCount' => $TestsCount,
            'commentsCount' => $CommentCount,
            'usersCountToday' => $usersCountToday,
            'sessionsCountToday' => $SessionsCountToday,
            'testsCountToday' => $TestsCountToday,
            'commentsCountToday' => $CommentCountToday,
        ];
    }


    /**
     *
     */
    public function settings()
    {
        return Settings::first();
//        return view('panel.settings.index', compact('settings'));
    }

    /**
     * @param Request $request
     * @return Application|ResponseFactory|Response
     */
    public function saveSettings(Request $request)
    {
        $settings = Settings::find(1);
        $settings->youtube_home_video = $request->youtube_home_video;
        $settings->payment_status = $request->payment_status;
        $settings->save();
        return response(null, 200);
//        return redirect()->back();

    }


}




