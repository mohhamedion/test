<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Hash;
use Validator;

class UserController extends Controller
{


    public function index()
    {
        return User::orderBy('id', 'DESC')->paginate();
//        return view('panel.users.index', compact('users'));
    }


    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|max:255',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);

        }
        $user = User::where('role', 'admin')->where('email', $request->email)->get()->last();
        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                Auth::attempt(['email' => $request->email, 'password' => $request->password]);
                return redirect()->route('panel');
            }
        }

        return redirect()->back();
    }


    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');

    }


}
