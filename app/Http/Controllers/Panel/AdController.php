<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Models\Ad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdController extends Controller
{

    /**
     *
     */
    public function index(){
        $table= Ad::all();
        return view('panel/ads/index',compact('table'));
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(){
        return view('panel/ads/create');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request){
        $data  = $request->post();
        $data['user_id'] = Auth::user()->id;
        Ad::create($data);
        return redirect()->back();
    }

    /**\
     * @param Ad $ad
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Ad $ad){
    $ad->delete();
    return redirect()->back();
    }


}
