<?php

namespace App\Http\Requests\TestSessions;

use Illuminate\Foundation\Http\FormRequest;

class StoreAnswerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'session_question_id'=>'required|int',
            'answer_ids'=>'required|array|exists:answers,id',
            'answer_ids.*'=>'int',
        ];
    }
}
