<?php

namespace App\Http\Requests\Tests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'level' => 'required|int',
            'title' => 'max:100',
            'description' => 'required|max:700',
            'category' => 'required|int',
            'second_per_question' => 'required|max:255',
            'questions_per_session'=>'min:2|int'
        ];
    }
}
