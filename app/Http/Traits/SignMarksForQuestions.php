<?php

namespace App\Http\Traits;

use App\Models\Answer;

trait SignMarksForQuestions
{


    private function signMarksForQuestions($sessionQuestion, $answerIds)
    {

        $correctAnswers = Answer::select('id')->where("question_id", $sessionQuestion->question_id)->where('correct', 1)->get();

        if (count($correctAnswers) > 0) {


            $correctAnswers = $this->objectToIndexArray($correctAnswers);

            $mark = 1 / count($correctAnswers);
            $countCorrectAnswersByUser = count(array_intersect($correctAnswers, $answerIds));


            $countIncorrectAnswersByUser = count($answerIds) - $countCorrectAnswersByUser;
            $diffrence = ($countCorrectAnswersByUser - $countIncorrectAnswersByUser);
            if ($diffrence < 0) {
                $diffrence = 0;
            }

            $finalMark = $mark * $diffrence;


            $sessionQuestion->mark = $finalMark;
            $sessionQuestion->save();
        }

        return true;
    }

    private function objectToIndexArray($assArray)
    {
        $result = [];
        foreach ($assArray as $array) {
            $result[] = $array->id;
        }
        return $result;
    }

}
