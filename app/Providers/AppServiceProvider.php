<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\SessionQuestion;
use App\Models\Question;
use App\Observers\SessionQuestionObserver;
use App\Observers\QuestionObserver;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
//        if ($this->app->isLocal()) {
//            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
//        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        SessionQuestion::observe(SessionQuestionObserver::class);
        Question::observe(QuestionObserver::class);
    }
}
