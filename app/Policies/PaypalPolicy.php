<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PaypalPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @param $purchasesable
     * @return bool
     */
    public function createPayment(User $user,$purchasesable): bool
    {
        return (int)$user->id===(int)$purchasesable->user_id;
    }



}
