<?php

namespace App\Policies;

use App\Models\Test;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TestPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

//    public function before(User $user, $ability)
//    {
//        if ($user->isAdministrator()) {
//            return true;
//        }
//    }

    public function update(User $user,Test $test): bool
    {
       return $user->id===$test->user_id;
    }

    public function publish(User $user,Test $test): bool
    {
        return $user->id===$test->user_id;
    }


}
