<?php

namespace App\Notifications;

use App\Models\Test;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewTest extends Notification
{
    use Queueable;

    private $test;

    private $category;

    /**
     * NewTest constructor.
     * @param $test
     */
    public function __construct(Test $test, $category)
    {
        $this->test = $test;
        $this->category = $category;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting('مرحبا!')
            ->line(  ' أختبار جديد في '.$this->category->category )
            ->action('اضغط هنا للاختبار'.'💻☕', url("/test/".$this->test->id))
            ->line('شكرا لأستخدامك منصة quizzhunter'.' 😁🥰');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'message' => $this->category->category . " أختبار جديد في ",
            'link' => '/tests/' . $this->test->id,
            'name' => 'test'
        ];
    }
}
