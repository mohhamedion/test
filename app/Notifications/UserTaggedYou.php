<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UserTaggedYou extends Notification
{
    use Queueable;
    private $test;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($test,$user)
    {
        //
        $this->test = $test;
        $this->user= $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('احد المستخدمين ذكرك في تعليق')
                    ->action('اضغط لمشاهدة التعليق', url('/test/'.$this->test->id))
                    ->line('شكرا لاستخدامك منصتنا');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase(): array
    {
        return [
            'message' => "تم ذكرك في تعليق",
            'link' => '/test/' . $this->test->id,
            'name' => 'test'
        ];
    }
}
