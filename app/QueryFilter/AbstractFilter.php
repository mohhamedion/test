<?php


namespace App\QueryFilter;


use Closure;
use Illuminate\Database\Eloquent\Builder;

abstract class AbstractFilter
{

    public $request;
    public  $where;

    public function __construct(){
        $this->request = request();
    }

    public function handle($request, Closure $next)
    {
        if (!$this->shouldApply()) {
            return $next($request);
        }

        $builder = $next($request);

        return $this->apply($builder);
    }

    abstract protected function shouldApply(): bool;

    abstract protected function apply(Builder $builder): Builder;
}
