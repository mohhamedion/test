<?php

namespace App\QueryFilter\Tests;

use App\QueryFilter\AbstractFilter;
use Illuminate\Database\Eloquent\Builder;

class FilterIfPruchesed extends AbstractFilter
{

    protected function shouldApply(): bool
    {

        return $this->request->user('api') ? true : false;
    }

    protected function apply(Builder $builder): Builder
    {
        return $builder->withCount('purchasesByUser');
    }
}
