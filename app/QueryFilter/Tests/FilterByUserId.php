<?php

namespace App\QueryFilter\Tests;

use App\QueryFilter\AbstractFilter;
use Illuminate\Database\Eloquent\Builder;

class FilterByUserId extends AbstractFilter
{

    protected function shouldApply(): bool
    {
        return $this->request->input('user_id') ? true : false;
    }

    protected function apply(Builder $builder): Builder
    {
        return $builder->where('user_id', $this->request->input('user_id'));
    }
}
