<?php


namespace App\Exceptions;


use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

abstract class AbstractException extends Exception
{
    /**
     * @return void
     */
    public function report(): void
    {
        //
    }

    /**
     * @return JsonResponse
     */
    public function render(): JsonResponse
    {
        return response()->json(['errors' => $this->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
