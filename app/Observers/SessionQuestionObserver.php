<?php

namespace App\Observers;

use App\Models\SessionQuestion;

class SessionQuestionObserver
{
    /**
     * Handle the session question "created" event.
     *
     * @param  \App\Models\SessionQuestion  $sessionQuestion
     * @return void
     */
    public function created(SessionQuestion $sessionQuestion)
    {

    }

    /**
     * Handle the session question "updated" event.
     *
     * @param  \App\Models\SessionQuestion  $sessionQuestion
     * @return void
     */
    public function updated(SessionQuestion $sessionQuestion)
    {
        $session = $sessionQuestion->session;
        $questionLeft = SessionQuestion::where('answered', false)->where('session_id', $sessionQuestion->session_id)->count();
        if ($questionLeft <= 0) {
            $session->going = false;
            $session->save();
        }

    }

    /**
     * Handle the session question "deleted" event.
     *
     * @param  \App\Models\SessionQuestion  $sessionQuestion
     * @return void
     */
    public function deleted(SessionQuestion $sessionQuestion)
    {
        //
    }

    /**
     * Handle the session question "restored" event.
     *
     * @param  \App\Models\SessionQuestion  $sessionQuestion
     * @return void
     */
    public function restored(SessionQuestion $sessionQuestion)
    {
        //
    }

    /**
     * Handle the session question "force deleted" event.
     *
     * @param  \App\Models\SessionQuestion  $sessionQuestion
     * @return void
     */
    public function forceDeleted(SessionQuestion $sessionQuestion)
    {
        //
    }
}
