<?php

namespace App\Observers;

use App\Models\TestSession;

class TestSessionObserver
{
    /**
     * Handle the test session "created" event.
     *
     * @param \App\Models\TestSession $testSession
     * @return void
     */
    public function created(TestSession $testSession)
    {
    }

    /**
     * Handle the test session "created" event.
     *
     * @param \App\Models\TestSession $testSession
     * @return void
     */
    public function creating(TestSession $testSession)
    {
        //turn off old sessions if exist
//        TestSession::where('going', true)->where('user_id', $testSession->user_id)->update(['going' => false]);

    }

    /**
     * Handle the test session "updated" event.
     *
     * @param \App\Models\TestSession $testSession
     * @return void
     */
    public function updated(TestSession $testSession)
    {
        //
    }

    /**
     * Handle the test session "deleted" event.
     *
     * @param \App\Models\TestSession $testSession
     * @return void
     */
    public function deleted(TestSession $testSession)
    {
        //
    }

    /**
     * Handle the test session "restored" event.
     *
     * @param \App\Models\TestSession $testSession
     * @return void
     */
    public function restored(TestSession $testSession)
    {
        //
    }

    /**
     * Handle the test session "force deleted" event.
     *
     * @param \App\Models\TestSession $testSession
     * @return void
     */
    public function forceDeleted(TestSession $testSession)
    {
        //
    }
}
