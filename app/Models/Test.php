<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Test extends Model
{
    use HasFactory;


    protected $fillable = [
        'level',
        'description',
        'second_per_question',
        'category',
        'user_id',
        'video',
        'title',
        'approved',
        'price',
        'questions_per_session'
    ];

    /**
     * @var mixed
     */

    public function category()
    {
        return $this->belongsTo(Category::class, 'category');
    }

    public function theCategory()
    {
        return $this->belongsTo(Category::class, 'category');
    }

    public function level()
    {
        return $this->belongsTo(Level::class, 'level');
    }

    public function questions()
    {
        return $this->hasMany(Question::class, 'test_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'test_id')->where('comment_id',null);
    }

    public function rates()
    {
        return $this->hasMany(TestRate::class, 'test_id');
    }


    public function sessions()
    {
        return $this->hasMany(TestSession::class, 'test_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function scopeAllowedByUser($query)
    {
        return $query->where('user_id', auth('api')->user()->id);
    }

    public function scopeAprovedAndPublished($query)
    {
        return $query->where('published', 1)->where('approved', 1);

    }




    public function purchases()
    {
        return $this->morphMany(Purchase::class, 'purchaseable');

    }

    public function purchasesByUser()
    {
        return $this
            ->purchases()
            ->where('buyer_id', auth('api')->user()->id);
    }
}
