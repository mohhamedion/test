<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    use HasFactory;
    protected $fillable = ['cost','buyer_id','test_id','attempts','seller_id'];
    protected $table = 'purchases';
//    public function test(){
//        return $this->hasOne(Test::class,'test_id');
//    }

    public function purchaseable(){
        return $this->morphTo();
    }

}
