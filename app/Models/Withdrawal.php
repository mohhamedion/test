<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use \Illuminate\Database\Eloquent\Relations\BelongsTo;
class Withdrawal extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function withdrawalRequest()
    {
        return $this->hasOne(WithdrawalRequest::class,'withdrawal_id');
    }
}
