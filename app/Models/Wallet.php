<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    use HasFactory;
    protected $fillable = ['user_id','coins','paypal_email'];
    protected $table = 'wallets';

    public function user(){
        return $this->belongsTo(User::class,'id');
    }
}
