<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use \Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Comment extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'test_id', 'comment','comment_id'];


    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function test(): BelongsTo
    {
        return $this->belongsTo(Test::class, 'test_id');
    }

    public function comment(): BelongsTo
    {
        return $this->belongsTo(__CLASS__, 'comment_id')->withDefault();
    }

    public function comments(): HasMany
    {
        return $this->hasMany(__CLASS__, 'comment_id');
    }

}
