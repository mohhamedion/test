(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[13],{

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/policy/policy.vue?vue&type=template&id=3f99f0f4&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/policy/policy.vue?vue&type=template&id=3f99f0f4& ***!
  \****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "container mt-5" }, [
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "large" } }, [
                _c("strong", [_vm._v("شروط استخدام موقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "large" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [
              _c("strong", [_vm._v("QuizzHunter")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v("تتغير شروط واحكام موقع ")
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "medium" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("،")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v(
                    " باستمرار فاحرص على مداومة زيارة هذه الصفحة كي تبقى عل اطلاع بالتغييرات، بدخولك واستعمالك لمنصتنا فانت"
                  )
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("توافق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ضمنيا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الشروط")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والاحكام")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الموضحة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هذه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الصفحة")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("كافة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الشروط")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والبنود")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المبينة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هذه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الصفحة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تتنافى")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("من")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("القوانين")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الجاري")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("العمل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("قوانين")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("انتهاك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الخصوصية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والملكية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الفكرية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والملكية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المشتركة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المعمول")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("دوليا")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(". ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ويمكنكم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("معرفة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المزيد")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هذا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عبر")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("زيارة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("صفحة")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v('"')])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("سياسة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الخصوصية")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v('".')])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [_c("br")]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("البنود")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("الواجب")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("عليك")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("الالتزام")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("بها")])
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v(":")])
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("(1) ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تقديم")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(":")])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("مرحبا ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بكم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("فيما")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يلي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الشروط")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والأحكام")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("التي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تخص")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("استخدامك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ودخولك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لصفحات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الموقع")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(":")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("دخولك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("واستخدامك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لموقع")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _vm._v("www.QuizzHunter.com ")
              ])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موافقة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("منك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("القبول")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ببنود")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وشروط")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هذه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الاتفاقية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والتي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تشمل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("كافة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("التفاصيل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أدناه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وهو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تأكيد")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لالتزامك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بالاستجابة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لمضمون")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هذه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الاتفاقية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الخاصة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بـموقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والمشار")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إليه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("فيما")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يلي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("باسم")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v('"')])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("نحن")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v('" ')])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والمشار")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("اليها أيضا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بالـ ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v('"')])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الموقع")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v('"')])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("باقي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بنود")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v('" ')])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الشروط")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والأحكام")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v('" ')])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وتعتبر")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هذه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الاتفاقية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("سارية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المفعول")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حال")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("دخولك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("للموقع")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [_c("br")]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v("(2) ")])
              ])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("العضوية")])
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v(":")])
              ])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v("1")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(". ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يحق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لأي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("شخص")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("استخدام")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الموقع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إذا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ألغيت")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عضويته")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("من")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("2. ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v(
                    "تسجيل العضوية على موقعنا يكون بغرض الاختبارات فقط وليس بنية او غرض اخر"
                  )
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "medium" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("3")])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(". ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يحق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لأي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عضو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ان")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يقوم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بفتح")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حسابين")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("آن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("واحد")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لأي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("سبب")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v("كان، ولإدارة")
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الموقع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الحق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بتجميد")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("او")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ايقاف")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("اي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حساب")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تراه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مخالفا")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("br"),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v("3) ")])
              ])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("الحسابات")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("والتزامات")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("التسجيل")])
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v(":")])
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("فور")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تقديم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("طلب")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("التسجيل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("للحصول")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عضوية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الموقع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تكون")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مطالباً")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بالإفصاح")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("معلومات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("محددة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("واختيار")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("اسم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مستخدم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وكلمة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مرور")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("سرية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لاستعمالها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عند")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الدخول")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("للموقع")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(". ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وعند")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تنشيط")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حسابك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ستعتبر")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عضوا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بالموقع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وبذلك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تكون")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("قد")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وافقت")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(":")])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("1. ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تكون")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مسؤولاً")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المحافظة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("سرية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("معلومات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حسابك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وكلمة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المرور")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("السرية")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(". ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وتكون")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بذلك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موافقاً")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إعلام")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حالاً")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بأي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("استخدام")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("غير")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مفوض")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("به")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لكلمة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("دخولك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حسابك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("اختراق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("آخر")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لمعلوماتك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("السرية")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("2. ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يكون")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بأي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حال")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("من")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الأحوال")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مسؤولاً")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("خسارة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("قد")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تلحق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بشكل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مباشر")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("غير")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مباشر")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("معنويا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ماديا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("نتيجة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("كشف")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("معلومات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("اسم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المستخدم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("كلمة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الدخول")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("3. ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بتسجيلك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عضوية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تعتبر")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المسؤول")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الحصري")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والوحيد")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("خسائر")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مباشرة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("غير")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مباشرة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("قد")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تلحق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بـموقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("نتيجة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("استخدام")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("غير")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("شرعي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مفوض")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لحسابك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("من")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("طرفك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("من")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("طرف")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("شخص")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("آخر")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حصل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مفاتيح")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الوصول")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إلى")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حسابك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بالموقع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("سواء")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("كان")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بتفويض")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("منك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بدون")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تفويض")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("4. ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أنت")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موافق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الإدلاء")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بمعلومات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حقيقية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v("وصحيحة ومحدثة")
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وكاملة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("نفسك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حسبما")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مطلوب")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("استمارة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("التسجيل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لموقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("5. ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تدرج")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ضمن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("اسم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المستخدم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("من")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تفاصيل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الاتصال")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("كعناوين")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("البريد")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الإلكتروني")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أرقام")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هواتفك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تفاصيل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("شخصية")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("كلمة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("او")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("اي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("كلمة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تدل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("اسم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقعنا")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("6. ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يلتزم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بالتعامل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("معلوماتك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الشخصية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وعناوين")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الاتصال")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بسريّة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تامة")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("7. ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("سوف")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تكون")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ملزماً")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بالحفاظ")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بيانات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("التسجيل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وتحديثها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("دائما")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("للإبقاء")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عليها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حقيقية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وصحيحة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وراهنة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وكاملة")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وإذا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ما")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أفصحت")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("معلومات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("غير")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حقيقية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("غير")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("صحيحة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("غير")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("راهنة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("غير")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("كاملة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("او")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مخالفة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لما")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("جاء")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("اتفاقية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الشروط")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والأحكام")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("فإن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لـموقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الحق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("كاملاً")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وقف")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تحديد")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إلغاء")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v("عضويتك وحسابك")
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الموقع")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وذلك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("دون")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الحاق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الأضرار")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بحقوق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الأخرى")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ووسائله")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المشروعة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("استرداد")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حقوقه")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("8. ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لـموقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "medium" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
          ])
        ]),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مطلق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الإرادة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وفي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وقت")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يجري")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تحقيقات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يراها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ضرورية")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("(")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مباشرة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عبر")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("طرف")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ثالث")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(") ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ويطالبك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بالإفصاح")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("معلومات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إضافية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وثائق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مهما")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("كان")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حجمها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لإثبات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هويتك و")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("/")])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ملكيتك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لأدواتك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المالية")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("9. ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حالة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عدم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الالتزام")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بأي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مما")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ورد")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أعلاه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("فإن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لإدارة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الحق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إيقاف")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إلغاء")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v("عضويتك وحجبك")
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الموقع")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(". ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ونحتفظ")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("كذلك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بالحق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إلغاء")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حسابات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("غير")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مؤكدة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وغير")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مثبتة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عمليات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حسابات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مر")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عليها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مدة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("طويلة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("دون")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("نشاط")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [_c("br")]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v("(4) ")])
              ])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("التعديل")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("على")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("الشروط")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("والأحكام")])
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v(":")])
              ])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v("1")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(". ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أنت")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تعلم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وموافق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يقوم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بأي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تعديل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("اتفاقية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الشروط")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والأحكام")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وبموجبه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تتضاعف")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("التزاماتك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تتضاءل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حقوقك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وفقاً")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لأي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تعديلات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("قد")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تجرى")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("اتفاقية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الشروط")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والأحكام")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("دون")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الحاجة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الى")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("اعلامك")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("2. ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أنت")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("توافق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ان")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يملك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بمطلق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("صلاحيته")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ودون")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تحمله")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المسؤولية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("القانونية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يجري")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تعديلات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أساسية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("فرعية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هذه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الاتفاقية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("دون")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يتطلب")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ذاك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موافقة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إضافية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("من")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("طرفك")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وذلك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وقت")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وبأثر")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("فوري")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("دون")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إلزام")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بأخبارك")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(", ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ويمكنك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("دائما")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("معرفة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("التعديلات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عبر")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الرجوع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لهذه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الصفحة")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("3. ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أنت")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("توافق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ان")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يعتبر")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إلكترونيا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تقنيا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("شبكة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الانترنت")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يتيح")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("للمسجلين")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v(" اختبار مهاراتهم في البرمجة")
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("4. ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("العضوية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الموقع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مجانية")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("5. ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v("لا يقدم موقع ")
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "medium" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter ")])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [
                _vm._v("اي رسوم مقابل التسجيل او الاستخدام")
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "medium" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("6")])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(". ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("للموقع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الحق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بعمل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v("تعديلات بما")
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يراه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مناسبا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وسيتم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الإعلان")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تعديل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("من")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("خلال")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مراسلتك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بريدك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الإلكتروني")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v("الموقع، وقد")
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تكون")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("التعديلات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مؤقتة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مستمرة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وعليك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الالتزام")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وفق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بنودها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حين")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يتم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الإعلان")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عنها")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [_c("br")]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("br"),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v("(5) ")])
              ])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("الخدمات")])
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(":")])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v(
                  "يوفر موقعنا منصة لاختبار القدرات للباحثين عن العمل أو لتحسين الخبرة في المجالات العديدة وخاصة في مجال البرمجة وهو ايضا موجه للـ "
                )
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "medium" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [_vm._v("HR ")])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _vm._v("في الشركات التي تبحث عن موظفين")
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "medium" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [_vm._v(", ")])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _vm._v("يوفر ")
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "medium" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [_vm._v("Quizzhunter ")])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _vm._v("بيانات جلسات الاختبار للشركات وإحصاءات عامة")
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "medium" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [_vm._v(", ")])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _vm._v("لتحسين انتقاء الموظفين")
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "medium" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [_c("br")]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [_c("br")]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v("(6) ")])
              ])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("معلوماتك")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("الشخصية")])
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v(":")])
              ])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("br")
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("1. ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("توافق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ان")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لموقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الحق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("التصرف")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("معلوماتك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ضمن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ما")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("محدد")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هذه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الصفحة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وفي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("صفحة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("سياسة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الخصوصية")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بشكل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("دائم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وغير")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("قابل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("للإلغاء")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وذلك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("من")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("خلال")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("التسجيل")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("او")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("استعمال")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("النماذج")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المخصصة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v("للتواصل والتسجيل")
                ])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عبر")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("رسالة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إلكترونية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("من")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("قنوات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الاتصال")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المتاحة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بالموقع")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(". ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وذلك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بهدف")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تشغيل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وترويج")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الموقع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وفق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("اتفاقية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إخلاء")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المسؤولية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وبيان")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الخصوصية")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("2")])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(". ")])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أنت الوحيد")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المسؤول")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المعلومات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("التي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("قمت")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بإرسالها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("نشرها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وينحصر")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("دور")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بالسماح")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بعرض")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هذه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المعلومات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("صفحات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الموقع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ومن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("خلال")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("قنواته")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الإعلانية")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [_c("br")]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v("(7")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v(") ")])
              ])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("تتعهد وتعلن وتضمن أنك")])
                ])
              ])
            ])
          ]),
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v(":")])
              ])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("أنك سوف")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تلتزم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بكافة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("القوانين")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المحلية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والدولية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المعمول")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هذا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الشأن")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وكذا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v("الشروط والأحكام")
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المعمول")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("شأن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("استخدام")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الموقع")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(". ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("كما")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("انه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("كعضو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مسجل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الموقع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("فأنك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("توافق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ضمنيا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v(
                    "كل الشروط والاحكام كما تضمن عدم التحايل او استخدام منصتنا لأي غرض غير قانوني كالاحتيال وغيره من الممارسات الممنوعة سواء على مستوى القوانين والتشريعات وعلى منصتنا"
                  )
                ])
              ])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("br")
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [_c("br")]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v("(8) ")])
              ])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("حقوق")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("الطبع")])
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v(":")])
              ])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v("1")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(". ")])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v("كافة المحتويات")
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("التي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يتضمنها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الموقع")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بما")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ذلك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وليس")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("محصوراً")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("النصوص")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("التصاميم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الجرافيكية")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الشعارات")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أيقونات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الأزرار")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الرموز")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المقاطع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الصوتية")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الأحمال")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الرقمية")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("البيانات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المجمّعة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والبرامج")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v("الإلكترونية")
                ])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ملك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وحقوقها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("محفوظة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إما")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("للـموقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("للجهة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المسؤولة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تصميم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وللمعدين")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لهذه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المحتويات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وللمفوضين")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وهي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("محمية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ضمن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حقوق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الطبع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المحفوظة")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والعلامات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("التجارية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وحقوق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وقوانين")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الملكية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الفكرية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والإبداعية")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [_c("br")]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v("(9) ")])
              ])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("العلامات")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("التجارية")])
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v(":")])
              ])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v("1")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(". ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الشركات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والعلامات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("التجارية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("التي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يتعامل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("معها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter ")])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("محمية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بحقوق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وقوانين")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ملكية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("العلامات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("التجارية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الدولية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والفكرية")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(". ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ولا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يمكنك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("استخدام")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("اسمائها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("او")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هويتها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("البصرية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("اي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("شيء")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("دون")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موافقة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مكتوبة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("من")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الشركة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المعنية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بالأمر")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(", ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("نفس")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الشيء")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ينطبق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على موقعنا")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [_c("br")]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("br"),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v("(10) ")])
              ])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("سرية")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("المعطيات")])
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v(":")])
              ])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v("1")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(". ")])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يتخذ موقع ")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter ")])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("معايير")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("(")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ملموسة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وتنظيمية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وتكنولوجية")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(") ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("للحماية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("من")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وصول")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("شخص")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("غير")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مفوض")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إلى")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("معلومات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هويتك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الشخصية")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وحفظها")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(". ")])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("2. ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "medium" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter ")])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ليس")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("له")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("سيطرة")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أفعال")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أي")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("طرف")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ثالث")])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _vm._v("، ")
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مثل")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("صفحات")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الانترنت")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الأخرى")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الموصولة")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بهذا")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الموقع")])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _vm._v("، ")
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أطراف")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ثالثة")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تدعي")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أنها")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تمثلك")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وتمثل")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("آخرين")])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("3")])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(". ")])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أنت تعلم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وموافق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("قد")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يستخدم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("معلوماتك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("التي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("زودته")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بها")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بهدف")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تقديم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الخدمات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ولإرسال")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("رسائل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تسويقية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لك")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وان")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بيان")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الخصوصية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هذا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الموقع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يضبط")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عمليات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الجمع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والمعالجة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والاستخدام")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والتحويل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لمعلومات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هويتك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الشخصية")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [_c("br")]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [_c("br")]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v("(11) ")])
              ])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("نقض")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("اتفاقية")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("الشروط")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("والأحكام")])
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v(":")])
              ])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("strong", [_vm._v("ان")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بحسب")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("اتفاقية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الشروط")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والأحكام")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وبحسب")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("القانون")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("قد")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يلجأ")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إلى")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وقف")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مؤقت")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("دائم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("سحب")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وإلغاء")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عضويتك و")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("/")])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تحديد")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إلغاء")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وصولك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إلى")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الموقع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("دون")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الإضرار")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بحقوقه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الأخرى")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ووسائله")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المشروعة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("استرداد")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حقوقك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حالة")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(":")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("1")])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(". ")])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v("إذا انتهكت اتفاقية الشروط والأحكام")
                ])
              ])
            ])
          ]),
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("2. ")])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v("إذا لم يكن بإمكان موقع ")
                ])
              ])
            ])
          ]),
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter ")])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v("التأكد من صحة أي من معلوماتك المقدمة إليه")
                ])
              ])
            ])
          ]),
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("3. ")])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v("إذا قرر موقع ")
                ])
              ])
            ])
          ]),
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter ")])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v("أن نشاطاتك قد تتسبب لك أو لمستخدمين آخرين ولـموقع ")
                ])
              ])
            ])
          ]),
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter ")])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v("في إشكالات قانونية")
                ])
              ])
            ])
          ]),
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("4")])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(". ")])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v("قد يلجأ موقع ")
                ])
              ])
            ])
          ]),
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _vm._v('QuizzHunter "')
              ])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v("بحسب تقييمه ")
                ])
              ])
            ])
          ]),
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v('" ')])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v(
                    "إلى إعادة نشاط المستخدمين الموقوفين، حيث أن المستخدم الذي أوقف نشاطه نهائياً أو سحبت عضويته، قد لا يكون بإمكانه التسجيل أو محاولة التسجيل في موقع "
                  )
                ])
              ])
            ])
          ]),
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter ")])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v(
                    "أو استخدام الموقع بأي طريقة كانت مهما كانت الظروف، لحين السماح له بإعادة نشاطه في موقع "
                  )
                ])
              ])
            ])
          ]),
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _vm._v("QuizzHunter. ")
              ])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v(
                    "ومع ذلك فإنك إن قمت بانتهاك اتفاقية الشروط والأحكام هذه، يحتفظ الموقع بحقه في استعادة أية مبالغ مستحقة للموقع عليك، وأي خسائر وأضرار تسببت بها لـموقع "
                  )
                ])
              ])
            ])
          ]),
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter ")])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v("كما أن له الحق باتخاذ الإجراءات القانونية و")
                ])
              ])
            ])
          ]),
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("/")])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v(
                    "أو اللجوء للمحاكم لرفع قضية قانونية ضدك حسبما يراه موقع "
                  )
                ])
              ])
            ])
          ]),
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter ")])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مناسباً")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("5")])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(". ")])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إن قيامك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("قيام")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("آخرين")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بانتهاك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الاتفاقية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هذه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يلزم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بالتنازل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حقه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("اتخاذ")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الإجراءات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المناسبة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لمثل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هذا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الفعل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ولغيره")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("من")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أفعال")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الانتهاك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المشابهة")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(". ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وموقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "medium" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
          ])
        ]),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يضمن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("اتخاذه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إجراءات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ضد")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("كل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الانتهاكات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لاتفاقية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الشروط")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والأحكام")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [_vm._v(" ")]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("br"),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v("(12) ")])
              ])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("محتويات")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("غير")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("قانونية")])
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v(":")])
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("1")])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(". ")])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v(
                    "كعضو مسجل في الموقع، ممنوع عليك الإعلان عن أرقام هواتفك أو بريدك الإلكتروني او اي شيء له صلة بالدخول الى حسابك من خلال أي جزء من الموقع او خارجه، ويعتبر ذلك بمثابة انتهاك للشروط والأحكام"
                  )
                ])
              ])
            ])
          ]),
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("br")
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v("(13) ")])
              ])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("التغذية")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("الرجعية")])
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v(":")])
              ])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("strong", [_vm._v("موقعنا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يشجع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("جميع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الأعضاء")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الموقع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لتقديم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("اقتراحات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تغذية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("رجعية")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("(")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("فيد")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("باك")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(")")])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وسوف")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تعرض")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الموقع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مكان")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تحدده")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إدارة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الموقع")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("br")
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v("(14) ")])
              ])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("إلغاء")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("الوصول و")])
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v("/")])
              ])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("أو")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("العضوية")])
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v(":")])
              ])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("strong", [_vm._v("بدون")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إلحاق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الضرر")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بحقوقه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الأخرى")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ووسائله")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المشروعة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لاسترداد")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حقوقه")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يمكن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لـموقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وقف")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إلغاء")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عضويتك و")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("/")])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وصولك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إلى")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الموقع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وقت")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وبدون")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إنذار")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ولأي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("سبب")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ودون")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تحديد")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ويمكنه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أيضا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إلغاء")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("اتفاقية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الشروط")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والأحكام")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هذه")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [_c("br")]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v("(15) ")])
              ])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("الضمان")])
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v(":")])
              ])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("strong", [_vm._v("موقع")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter ")])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("لا ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("قد")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يقدم اي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ضمانات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عن")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v(
                  " الاستمرار الدائم للخدمة او عن مطابقتها لأية معاييراو عن اي معطيات تدخلونها للموقع حيث لا نتحمل اي مسؤولية قانونية في حال ضياع بعض المعلومات عن الاعضاء"
                )
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [_c("br")]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v("(16) ")])
              ])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("تحديد")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("المسؤوليات")])
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v(":")])
              ])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("strong", [_vm._v("بحسب")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ما")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مسموح")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("القانون")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("فإن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقعنا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وموظفيه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ومدراءه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ووكلائه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والمتفرعين")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عنه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والمجهزين")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("له")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يكونوا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مسؤولين")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("خسارة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مباشرة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("غير")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مباشرة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أعطال")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تنشأ")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("استخدامك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("للموقع")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(". ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إذا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تكن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("راض")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الموقع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("من")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("محتوياته")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("فالحل هو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بعدم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("استمرارك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("باستخدامه")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("علاوة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ذلك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("فأنت")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موافق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("استخدام")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("غير")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مفوض")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("للموقع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وخدماته")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بسبب")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إهمالك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("سوف")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يتسبب")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بإلحاق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الأذى")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بالـموقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وعليه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("سيضطر")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الموقع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حينها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إلى")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("اللجوء")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إلى")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الشروط")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والأحكام")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [_c("br")]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v("(17) ")])
              ])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("الأمان")])
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v(":")])
              ])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("strong", [_vm._v("أنت")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موافق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("استخدامك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الامن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لموقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الحفاظ")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("امن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مدراءه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وموظفيه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ووكلائه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ومجهزيه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ووقايتهم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("من")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ضرر")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("قد")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يلحق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بهم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("جراء")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مطالبات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وخسائر")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وأعطال")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وتكاليف")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ونفقات")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تحدث")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بسبب")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("انتهاكك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لاتفاقية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الشروط")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والأحكام")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("خرقك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لأي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("قانون")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تعديلات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تعدي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حقوق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أطراف")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ثالثة")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [_c("br")]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [_c("br")]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v("(18) ")])
              ])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("العلاقة")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("والإشعارات")])
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v(":")])
              ])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("strong", [_vm._v("لا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تتضمن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("من")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بنود")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("اتفاقية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الشروط")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والأحكام")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إشارة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إلى")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وجود")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("شراكة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بينك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وبين")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(". ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وأنت")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ليس")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("سلطة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إلزام")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بأي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حال")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("من")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الأحوال")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وأن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إشعارات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تود")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إرسالها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لـموقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عليك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إرسالها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بالبريد")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الإلكتروني")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يقوم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بالرد")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الرسالة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v("الإلكترونية")
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(". ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أنت")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تعرف")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وموافق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إشعارات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ترسل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إليك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("من")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("سوف")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تعلن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الموقع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بواسطة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("البريد")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الإلكتروني")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الذي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("زودتنا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("به")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("خلال")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عملية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("التسجيل")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [_vm._v(" ")]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("br"),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v("(19) ")])
              ])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("تحويل")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("الحقوق")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("والالتزامات")])
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v(":")])
              ])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("strong", [_vm._v("أنت")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هنا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تمنح")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الحق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تحويل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("جزء")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("كل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حقوقه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ومنافعه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والتزاماته")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ومسؤولياته")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إلى")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أطراف")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أخرى")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تعمل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("معه")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("دون")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الحاجة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("للرجوع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إليك")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وذلك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بحسب")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("نصوص")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("اتفاقية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الشروط")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والحكام")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وموقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ملتزم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بإشعارك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مثل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هذه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("التحويلات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إذا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حصلت")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وكذلك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بالنشر")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الموقع")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [_c("br")]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v("(20) ")])
              ])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("معلومات")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("عامة")])
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v(":")])
              ])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("strong", [_vm._v("إذا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("كانت")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("فقرة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("واردة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("اتفاقية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الشروط")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والأحكام")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هذه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("غير")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("صالحة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ملغاة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أنها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لأي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("سبب")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تعد")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("نافذة")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("فإن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مثل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هذه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الفقرة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تلغي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("صلاحية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بقية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الفقرات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الواردة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الاتفاقية")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(". ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هذه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الاتفاقية")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("(")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والتي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تعدل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بين")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حين")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وآخر")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بحسب")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بنودها")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(") ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تضع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("كافة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الخطوط")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("العريضة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("للتفاهم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والاتفاق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بينك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وبين")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الاعتبار")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لما")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يلي")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(":")])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("-")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ليس")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("من")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("شخص")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يكون")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("طرفاً")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هذه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الاتفاقية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يفرض")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بنود")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("شروط")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("فيها")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("-")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إذا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تمت")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ترجمة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("اتفاقية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الشروط")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والأحكام")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لأي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لغة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أخرى")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("سواء")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الموقع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بطرق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أخرى")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("فإن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("النص")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("العربي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يظل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("السائد")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("br"),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v("(21) ")])
              ])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("القانون")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("والتشريع")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("الحاكمان")])
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v(":")])
              ])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("strong", [_vm._v("اتفاقية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الشروط")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والأحكام")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هذه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("محكومة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ومصاغة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بحسب")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v("القوانين الجاري بها العمل")
                ])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وهي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("خاضعة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تماماً")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وكلياً")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("للتشريع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المعمول")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("به")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المحاكم")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هذه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الفقرة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("توجد")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بديلاً")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يتم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("اللجوء")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إليه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حال")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("انتهاء")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مفعول")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("اتفاقية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الشروط")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والأحكام")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هذه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إلغاؤها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لأي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("سبب")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "medium" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("كان")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "medium" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [_c("br")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/policy/policy.vue":
/*!***************************************************!*\
  !*** ./resources/js/components/policy/policy.vue ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _policy_vue_vue_type_template_id_3f99f0f4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./policy.vue?vue&type=template&id=3f99f0f4& */ "./resources/js/components/policy/policy.vue?vue&type=template&id=3f99f0f4&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _policy_vue_vue_type_template_id_3f99f0f4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _policy_vue_vue_type_template_id_3f99f0f4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/policy/policy.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/policy/policy.vue?vue&type=template&id=3f99f0f4&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/policy/policy.vue?vue&type=template&id=3f99f0f4& ***!
  \**********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_policy_vue_vue_type_template_id_3f99f0f4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./policy.vue?vue&type=template&id=3f99f0f4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/policy/policy.vue?vue&type=template&id=3f99f0f4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_policy_vue_vue_type_template_id_3f99f0f4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_policy_vue_vue_type_template_id_3f99f0f4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);