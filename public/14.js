(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[14],{

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/privacy/privacy.vue?vue&type=template&id=498c4ff4&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/privacy/privacy.vue?vue&type=template&id=498c4ff4& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "container mt-5 mb-5" }, [
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "large" } }, [
                _c("strong", [_vm._v("سياسة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "large" } }, [
                _c("strong", [_vm._v("الخصوصية ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "large" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v("- ")])
              ])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "large" } }, [
                _c("strong", [_vm._v("موقع ")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "large" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v("QuizzHunter")])
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v(
                    "نقدر مخاوفكم واهتمامكم بشأن خصوصية بياناتكم على شبكة الإنترنت وعلى موقع "
                  )
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("u", [_vm._v("www.quizzhunter.com")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v(
                    "لقد تم إعداد هذه السياسة لمساعدتكم في تفهم طبيعة البيانات التي نقوم بتجميعها عنكم عند زيارتكم لموقعنا على شبكة الانترنت وكيفية تعاملنا مع هذه البيانات الشخصية"
                  )
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "small" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("باستخدامك")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _vm._v(" ل")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("انت")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("توافق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("كل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بنود")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هذه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الصفحة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وعلى")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بنود")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("صفحة")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v('"')])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("شروط")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والاحكام")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v('" ')])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الموضحة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v("على الموقع، وتلزمك هذه البنود")
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بتقبل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("كافة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الشروط")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والبنود")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المطبقة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عليك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مند")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("دخولك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("للموقع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الى")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حين")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الخروج")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("منه")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [_c("br")]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تلتزم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إدارة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الموقع")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وفق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v("القوانين المنظمة")
                ])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ووفق")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("قوانين")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حماية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المعطيات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والتي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تقرها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المنظمات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الدولية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بعدم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("كشف")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("معلومات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("شخصية")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المشترك")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("/")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المشتري")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مثل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("العنوان")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("رقم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الهاتف")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عنوان")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("البريد")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الإلكتروني")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وغيرها")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(". ")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("br"),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ويلتزم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بعدم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تبادل")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تداول")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("من")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تلك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المعلومات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بيعها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لأي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("طرف")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("آخر")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("طالما")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("كان")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ذلك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("حدود")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("قدرات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إدارة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الموقع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الممكنة")])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _vm._v("، ")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ولن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يُسمح")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بالوصول")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إلى")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المعلومات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("إلا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("للأشخاص")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المؤهلين")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والمحترفين")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الذين")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يشرفون")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [_c("br")]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("اخلاء")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("المسؤولية")])
                ])
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يقر")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المستخدم")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("/")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الزائر")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لموقعنا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("انه")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المسؤول")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الحصري")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والوحيد")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("على")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("طبيعة")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v("الاستخدام لموقعنا وعلى سلامة")
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المعطيات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والمعلومات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("التي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يدخلها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("او")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يقدمها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("للموقع")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(", ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("وتخلي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v("ادارة موقع ")
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مسؤوليتها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("من")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("كل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ضرر")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("او")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تسريب")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("او")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("اختلال")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("او")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("مصاريف")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يتكبدها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("المستخدم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أو")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يتعرض")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("جراء")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("استخدام")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("موقع ")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [_c("br")]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("التصفح")])
                ])
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v("لا يقوم موقع ")
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "small" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter ")])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بتجميع")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("اي")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("معطيات")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عنك")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("أثناء")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تصفحك")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("للموقع")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("(")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("دون")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تسجيل")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(") ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("كملفات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الكوكيز")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("(Cockies) ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("الخاص")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("او")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("اي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("معلومات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("اخرى")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("دون")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ارادتك")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(", ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لكن")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("جراء")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("استخادمنا")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("لخدمة")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _vm._v("Google Analytics ")
              ])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("كطرف")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("ثالت")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("قد")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("يتم")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تجميع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("بضع")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("معلومات")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("عنك")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("والتي")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("تحددها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("جوجل")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("في")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("صفحتها")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [_vm._v("هذه")])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("- ")])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#0563c1" } }, [
            _c("u", [
              _c(
                "a",
                {
                  attrs: { href: "https://policies.google.com/privacy?hl=en" }
                },
                [
                  _c("span", { staticStyle: { color: "#0563c1" } }, [
                    _c("span", { staticStyle: { "font-family": "Calibri" } }, [
                      _c("span", { staticStyle: { "font-size": "small" } }, [
                        _c("span", { attrs: { lang: "ar-SA" } }, [
                          _vm._v("سياسة الخصوصية")
                        ])
                      ])
                    ])
                  ])
                ]
              )
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [_c("br")]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("عنوان بروتوكول شبكة الإنترنت ")])
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "small" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [
              _c("strong", [_vm._v("(")])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v("IP)")])
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v("في أي وقت تزور فيه اي موقع انترنت بما فيها موقع ")
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "small" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter ")])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [
                _vm._v(
                  "سيقوم السيرفر المضيف بتسجيل عنوان بروتوكول شبكة الإنترنت "
                )
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "small" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [_vm._v("(")])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("IP")])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(") ")])
            ])
          ]),
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v(
                    "الخاص بك، تاريخ ووقت الزيارة ونوع متصفح الإنترنت الذي تستخدمه والعنوان "
                  )
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v("URL")])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { attrs: { lang: "ar-SA" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v(
                    "الخاص بأي موقع من مواقع الإنترنت التي تقوم بإحالتك إلى الى هذا الموقع على الشبكة"
                  )
                ])
              ])
            ])
          ]),
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [_c("br")]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [
                    _vm._v("الروابط الخارجية والاعلانات على الموقع")
                  ])
                ])
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v(
                    "قد يشتمل موقعنا على روابط لمواقع أخرى على شبكة الإنترنت"
                  )
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "small" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [_vm._v(". ")])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [
                _vm._v("او اعلانات من شركات اعلانية مثل ")
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _vm._v("Google AdSense ")
              ])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v(
                    "ولا نعتبر مسؤولين عن أساليب تجميع البيانات من قبل تلك المواقع"
                  )
                ])
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _vm._v("،")
              ])
            ])
          ]),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v(
                    " يمكنك الاطلاع على سياسات الخصوصية والمحتويات الخاصة بتلك المواقع التي يتم الدخول إليها من خلال أي رابط ضمن هذا الموقع"
                  )
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v(
                    "قد نستعين بشركات إعلانية او أطراف ثالثة لعرض الإعلانات او تقديم خدمات ويحق لهده الأطراف جمع بعض المعلومات عندما تزور موقعنا"
                  )
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "small" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [_vm._v(". (")])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [
                _vm._v(
                  "باستثناء الاسم أو العنوان أو عنوان البريد الإلكتروني أو رقم الهاتف"
                )
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "small" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [_vm._v(")")])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [
                _vm._v(
                  "، وذلك من أجل تقديم إعلانات حول البضائع والخدمات التي تهمك"
                )
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "small" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [_c("br")]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("إفشاء المعلومات ")])
                ])
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v(
                    "سنحافظ في كافة الأوقات على خصوصية وسرية كافة البيانات الشخصية التي نتحصل عليها"
                  )
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "small" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [_vm._v(". ")])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [
                _vm._v(
                  "ولن يتم إفشاء هذه المعلومات إلا إذا كان ذلك مطلوباً بموجب أي قانون أو مذكرة قضائية، وفقط عندما نعتقد بحسن نية أن مثل هذا الإجراء سيكون مطلوباً أو مرغوباً فيه للتمشي مع القانون"
                )
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [_vm._v("،")])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [
                _vm._v(
                  " أو للدفاع أو حماية حقوق الملكية الخاصة بهذا الموقع أو الجهات المستفيدة منه"
                )
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [_c("br")]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("التسجيل")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("والعضوية")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("على")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("موقعنا")])
                ])
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v("يختار المشترك كلمة سر ")
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "small" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [_vm._v("/ ")])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [
                _vm._v(
                  "مرور لحسابه، وسيُدخل عنوانا بريديا خاصا به لمراسلته عليه، وتكون مسؤولية حماية كلمة السر هذه وعدم مشاركتها أو نشرها على المشترك، وفي حال حدوث أي معاملات باستخدام كلمة السر هذه فسيتحمل المشترك كافة المسؤوليات المترتبة على ذلك، دون أدني مسؤولية على موقع "
                )
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "small" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter")])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v("يتحمل المشترك")
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "small" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [_vm._v("/ ")])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [
                _vm._v(
                  "الزائر كامل المسؤولية عن جميع المعطيات الخاصة به، التي يرفعها وينشرها عبر موقعنا"
                )
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v(
                    "يلتزم المشترك بعدم محاولة ولوج او قرصنة او تعديل أي معلومات عنه او عن أعضاء اخرين او عن الإدارة ليس له صلاحية في ولوجها وفي حال تم ذلك يبقى للمتجر وادارته كامل الحقوق في المتابعة قضائيا"
                  )
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "small" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v("يلتزم المشترك بكل شروط الاستخدام، وبألا يستعمل موقع ")
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "small" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [_vm._v("QuizzHunter ")])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [
                _vm._v(
                  "او أي شيء يدخل ضمن موقعنا في أي شيء يخالف الشريعة الإسلامية بأي شكل من الأشكال، أو في أغراض غير قانونية، على سبيل المثال لا الحصر، مثل"
                )
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "small" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [_vm._v(": ")])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [
                _vm._v(
                  "القرصنة ونشر وتوزيع مواد أو برامج منسوخة، أو الخداع والتزوير أو الاحتيال أو التهديد أو إزعاج أي شخص أو شركة أو جماعة أو نشر فيروسات أو ملفات تجسس أو وضع روابط إلى مواقع تحتوي على مثل هذه المخالفات"
                )
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "small" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [_vm._v("..")])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v(
                    "يمنع انتهاك حقوق الملكية الفكرية أو تشويه سمعة شخص أو مؤسسة أو شركة أو تعمد نشر أي معلومات تسبب ضررا لشركة أو شخص أو دولة أو جماعة وعدم وضع مواد قرصنة وبرامج مسروقة وجميع ما يخالف الأعراف والقوانين المنظمة، ويكون المشترك مسؤولا مسؤولية كاملة عن كل ما يقدمه عبر حسابه على الموقع"
                  )
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "small" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [_c("br")]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("التعديلات على البنود")])
                ])
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v(
                    "نحتفظ بالحق في تعديل بنود وشروط سياسة سرية وخصوصية المعلومات إن لزم الأمر ومتى كان ذلك ملائماً"
                  )
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "small" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [_vm._v(". ")])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [
                _vm._v(
                  "سيتم تنفيذ التعديلات هنا او على صفحة الشروط والأحكام وقد يتم او لا يتم بصفة مستمرة إخطارك بالبيانات التي حصلنا عليها"
                )
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [_vm._v("،")])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [
                _vm._v(
                  " وكيف سنستخدمها والجهة التي سنقوم بتزويدها بهذه البيانات"
                )
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ]),
        _c("br"),
        _vm._v(" "),
        _c("br")
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("الاتصال بنا ")])
                ])
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v(
                    "يمكنكم الاتصال بنا عند الحاجة من خلال الضغط على رابط اتصل بنا المتوفر في روابط موقعنا او الارسال الى بريدنا الالكتروني"
                  )
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [_c("br")]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("اخيرا")])
                ])
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _vm._v(
                    "إن مخاوفك واهتمامك بشأن سرية وخصوصية البيانات تعتبر مسألة في غاية الأهمية بالنسبة لنا"
                  )
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "small" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [_vm._v(". ")])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "ar-SA" } }, [
                _vm._v(
                  "نحن نأمل أن يتم تبديد كل مخاوفك وتوضيح مسارات كافة معطياتك من خلال هذه السياسة"
                )
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
          _c("span", { staticStyle: { "font-size": "small" } }, [
            _c("span", { attrs: { lang: "en-US" } }, [_vm._v(".")])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [_c("br")]),
      _vm._v(" "),
      _c("p", { attrs: { dir: "rtl", align: "right" } }, [
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("في")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("حال")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("كنت")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("غير")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("راضي")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("عن")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("أي")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("شيء")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("ضمن")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("هذه")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("الصفحة")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("او")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("في")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("صفحة")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("شروط")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("والاحكام")])
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v(", ")])
              ])
            ])
          ])
        ]),
        _c("span", { attrs: { lang: "ar-SA" } }, [
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("المرجوا")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("التوقف")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("عن")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("استخدام")])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("span", { staticStyle: { color: "#000000" } }, [
            _c("span", { staticStyle: { "font-family": "Calibri" } }, [
              _c("span", { staticStyle: { "font-size": "small" } }, [
                _c("span", { attrs: { lang: "ar-SA" } }, [
                  _c("strong", [_vm._v("موقعنا")])
                ])
              ])
            ])
          ])
        ]),
        _c("span", { staticStyle: { color: "#000000" } }, [
          _c("span", { staticStyle: { "font-family": "Calibri, serif" } }, [
            _c("span", { staticStyle: { "font-size": "small" } }, [
              _c("span", { attrs: { lang: "en-US" } }, [
                _c("strong", [_vm._v(".")])
              ])
            ])
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/privacy/privacy.vue":
/*!*****************************************************!*\
  !*** ./resources/js/components/privacy/privacy.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _privacy_vue_vue_type_template_id_498c4ff4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./privacy.vue?vue&type=template&id=498c4ff4& */ "./resources/js/components/privacy/privacy.vue?vue&type=template&id=498c4ff4&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _privacy_vue_vue_type_template_id_498c4ff4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _privacy_vue_vue_type_template_id_498c4ff4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/privacy/privacy.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/privacy/privacy.vue?vue&type=template&id=498c4ff4&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/privacy/privacy.vue?vue&type=template&id=498c4ff4& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_privacy_vue_vue_type_template_id_498c4ff4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./privacy.vue?vue&type=template&id=498c4ff4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/privacy/privacy.vue?vue&type=template&id=498c4ff4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_privacy_vue_vue_type_template_id_498c4ff4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_privacy_vue_vue_type_template_id_498c4ff4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);