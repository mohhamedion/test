(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[12],{

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/fortesters/fortesters.vue?vue&type=template&id=c9d2f058&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/fortesters/fortesters.vue?vue&type=template&id=c9d2f058& ***!
  \************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container height500px" }, [
    _c("h2", { staticClass: "text-center mt-2" }, [
      _vm._v("شارك في تطوير المجتمع العربي")
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "rtl text-right pt-5 " }, [
      _c("h3", { staticClass: "weight300" }, [_vm._v("هل انت صانع محتوى؟")]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "\n            في حال كنت صانع محتوى تعليمي , تستطيع انشاء اختبار مدفوع أو مجاني على موقع\n            QuizzHunter وارفاق الفيديو الخاص بالأختبار من اليوتيوب.\n        "
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "بهذه الطريقة , ستستطيع التعامل مع متابعيك بشكل اقرب, وافضل. وتلقي الدعم المادي منهم , وسيزيد من نتيجة الدروس التي تنشأها."
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "في حال كانت دروسك ذو جودة عالية, سيزيد من زياراتك على القناة من رواد هذا الموقع"
        )
      ]),
      _vm._v(" "),
      _vm._m(0),
      _vm._v(" "),
      _vm._m(1),
      _vm._v(" "),
      _c("ul", { staticClass: "mt-2" }, [
        _c("li", [
          _c(
            "div",
            { staticClass: "mt-3" },
            [
              _c("router-link", { attrs: { to: { name: "addTest" } } }, [
                _c("button", { staticClass: "buttonTry" }, [
                  _vm._v("انشاء اختبار")
                ])
              ])
            ],
            1
          )
        ]),
        _vm._v(" "),
        _c("li", { staticClass: "mt-2" }, [
          _vm._v(
            "\n                1-اضغط على زر, اضافة فيديو يوتيوب , ثم انشأ الاختبار\n            "
          )
        ]),
        _vm._v(" "),
        _c("li", [
          _vm._v(
            "\n                2-سيتوجب عليك تكمله الاختبار بالأسألة, ثم اطلاق الاختبار\n            "
          )
        ]),
        _vm._v(" "),
        _c("li", [
          _vm._v(
            "\n                2-تستطيع وضع 10 اسألة مثلا, وتحديد 5 فقط خلال الاختبار, حتى تظهر اسألة عشوائية في كل مرة\n            "
          )
        ]),
        _vm._v(" "),
        _c("li", [
          _vm._v(
            "\n                3-انشر الاختبار او ارفقه في الوصف على درس اليوتيوب بتعديل الفيديو الاصلي\n            "
          )
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "mt-4" }, [
      _c("img", {
        staticClass: "w-75",
        attrs: { src: "/fortestcreators/testpage.PNG", alt: "" }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "mt-4" }, [
      _c("h3", { staticClass: "weight300" }, [
        _vm._v("كيف تنشأ اختبار وترفق الدرس؟")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/fortesters/fortesters.vue":
/*!***********************************************************!*\
  !*** ./resources/js/components/fortesters/fortesters.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _fortesters_vue_vue_type_template_id_c9d2f058___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./fortesters.vue?vue&type=template&id=c9d2f058& */ "./resources/js/components/fortesters/fortesters.vue?vue&type=template&id=c9d2f058&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _fortesters_vue_vue_type_template_id_c9d2f058___WEBPACK_IMPORTED_MODULE_0__["render"],
  _fortesters_vue_vue_type_template_id_c9d2f058___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/fortesters/fortesters.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/fortesters/fortesters.vue?vue&type=template&id=c9d2f058&":
/*!******************************************************************************************!*\
  !*** ./resources/js/components/fortesters/fortesters.vue?vue&type=template&id=c9d2f058& ***!
  \******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_fortesters_vue_vue_type_template_id_c9d2f058___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./fortesters.vue?vue&type=template&id=c9d2f058& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/fortesters/fortesters.vue?vue&type=template&id=c9d2f058&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_fortesters_vue_vue_type_template_id_c9d2f058___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_fortesters_vue_vue_type_template_id_c9d2f058___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);