(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[8],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/test/AddTest.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/test/AddTest.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _ux_ui_errors_invalid_feedback__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../ux-ui/errors/invalid-feedback */ "./resources/js/components/ux-ui/errors/invalid-feedback.vue");
/* harmony import */ var _components_testForm__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/testForm */ "./resources/js/components/test/components/testForm.vue");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
 // import {mapGetters, mapActions} from "vuex";



/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    InvalidFeedback: _ux_ui_errors_invalid_feedback__WEBPACK_IMPORTED_MODULE_1__["default"],
    TestForm: _components_testForm__WEBPACK_IMPORTED_MODULE_2__["default"]
  },
  data: function data() {
    return {
      error: {},
      details: {
        level: null,
        title: null,
        description: null,
        category: null,
        second_per_question: null,
        questions_per_session: null,
        video: null,
        price: null
      },
      testDraft: {
        id: 123123,
        category: {
          category: 'draft',
          image: '/logo/logo.png'
        },
        level: {
          level: 'draft'
        },
        totalRate: 0,
        published: 0
      },
      draftTests: []
    };
  },
  methods: {
    // ...mapActions("categories", ["getHttpCategories"]),
    // ...mapActions("levels", ["getHttpLevels"]),
    createTest: function createTest(data) {
      var _this = this;

      axios__WEBPACK_IMPORTED_MODULE_0___default.a.post("/api/tests", data).then(function (response) {
        console.log(response.data);

        _this.$router.push({
          name: "addQuestion",
          params: {
            test_id: response.data[0].id
          }
        }); // this.appendNewDraftTest(response.data[0]);


        _this.details = {};
      })["catch"](function (error) {
        _this.error = _objectSpread({}, error.response.data.errors);
      });
    },
    getDraftTest: function getDraftTest() {
      var _this2 = this;

      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("/api/tests/getDraftTests").then(function (response) {
        _this2.draftTests = response.data;
      })["catch"](function (err) {});
    },
    appendNewDraftTest: function appendNewDraftTest(newTest) {
      this.draftTests.unshift(newTest);
    },
    deleteDraftTest: function deleteDraftTest(test_id, index) {
      var _this3 = this;

      axios__WEBPACK_IMPORTED_MODULE_0___default.a["delete"]("/api/tests/deleteDraftTest/" + test_id).then(function (response) {
        _this3.draftTests.splice(index, 1);
      })["catch"](function (err) {});
    }
  },
  computed: {// ...mapGetters("categories", ["getCategories"]),
    // ...mapGetters("levels", ["getLevels"])
  },
  mounted: function mounted() {
    // this.getHttpCategories();
    // this.getHttpLevels();
    this.getDraftTest();
  },
  created: function created() {}
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/test/AddTest.vue?vue&type=template&id=1f4b8501&":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/test/AddTest.vue?vue&type=template&id=1f4b8501& ***!
  \***************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container" }, [
    _c("div", { staticClass: "row" }, [
      _vm.draftTests.length > 0
        ? _c(
            "div",
            { staticClass: "draft col-md-5 col-12" },
            [
              _c("h2", { staticClass: "text-right" }, [
                _vm._v("الاختبارات المعلقة")
              ]),
              _vm._v(" "),
              _vm._l(_vm.draftTests, function(test, index) {
                return _c(
                  "div",
                  {
                    key: test.id,
                    staticClass: "bg-white  form-inline p-3 shadowBox mb-2"
                  },
                  [
                    _c("li", { staticClass: "col-4" }, [
                      _vm._v(_vm._s(test.category.category))
                    ]),
                    _vm._v(" "),
                    _c("li", { staticClass: "col-2" }, [
                      _vm._v(_vm._s(test.level.level))
                    ]),
                    _vm._v(" "),
                    _c("li", { staticClass: "col-3" }, [
                      _vm._v(_vm._s(test.second_per_question) + "s")
                    ]),
                    _vm._v(" "),
                    _c("li", { staticClass: "col-1" }, [
                      _c(
                        "span",
                        {
                          staticClass: "badge-danger badge pointer",
                          on: {
                            click: function($event) {
                              return _vm.deleteDraftTest(test.id, index)
                            }
                          }
                        },
                        [_vm._v("X")]
                      )
                    ]),
                    _vm._v(" "),
                    _c(
                      "li",
                      { staticClass: "col-2" },
                      [
                        _c(
                          "router-link",
                          {
                            attrs: {
                              to: {
                                name: "addQuestion",
                                params: { test_id: test.id }
                              }
                            }
                          },
                          [_vm._v("أضافة اسألة\n                    ")]
                        )
                      ],
                      1
                    )
                  ]
                )
              })
            ],
            2
          )
        : _vm._e(),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "  col-md-7 col-12",
          class: _vm.draftTests.length > 0 ? "" : "col-md-12"
        },
        [
          _c("TestForm", {
            attrs: {
              details: this.details,
              error: this.error,
              postFunction: this.createTest
            }
          })
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/test/AddTest.vue":
/*!**************************************************!*\
  !*** ./resources/js/components/test/AddTest.vue ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _AddTest_vue_vue_type_template_id_1f4b8501___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AddTest.vue?vue&type=template&id=1f4b8501& */ "./resources/js/components/test/AddTest.vue?vue&type=template&id=1f4b8501&");
/* harmony import */ var _AddTest_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AddTest.vue?vue&type=script&lang=js& */ "./resources/js/components/test/AddTest.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _AddTest_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _AddTest_vue_vue_type_template_id_1f4b8501___WEBPACK_IMPORTED_MODULE_0__["render"],
  _AddTest_vue_vue_type_template_id_1f4b8501___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/test/AddTest.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/test/AddTest.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./resources/js/components/test/AddTest.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AddTest_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./AddTest.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/test/AddTest.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AddTest_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/test/AddTest.vue?vue&type=template&id=1f4b8501&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/test/AddTest.vue?vue&type=template&id=1f4b8501& ***!
  \*********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AddTest_vue_vue_type_template_id_1f4b8501___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./AddTest.vue?vue&type=template&id=1f4b8501& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/test/AddTest.vue?vue&type=template&id=1f4b8501&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AddTest_vue_vue_type_template_id_1f4b8501___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AddTest_vue_vue_type_template_id_1f4b8501___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);