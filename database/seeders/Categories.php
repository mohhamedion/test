<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;
class categories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Category::truncate();

        Category::firstOrCreate(['category'=>"Javascript",'image'=>'https://habrastorage.org/webt/9t/29/hx/9t29hxcu4fftzxzxwrr-qakimny.jpeg']);
        Category::firstOrCreate(['category'=>"Php",'image'=>"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSlKW7jviISXnLDd1yrUX523QMz8vKWBBu4Bw&usqp=CAU"]);
        Category::firstOrCreate(['category'=>"Python",'image'=>"https://i2.wp.com/itc.ua/wp-content/uploads/2018/09/3-1.jpg"]);
        Category::firstOrCreate(['category'=>"HTML",'image'=>"https://newdohod.ru/images/WEB/html/html.jpg"]);
        Category::firstOrCreate(['category'=>"CSS",'image'=>"https://it-black.ru/wp-content/uploads/2019/04/nasledovanie_css.png"]);
        Category::firstOrCreate(['category'=>"Vue",'image'=>"https://media.proglib.io/wp-uploads/2018/07/1_qnI8K0Udjw4lciWDED4HGw.png"]);
        Category::firstOrCreate(['category'=>"React",'image'=>"https://reactjs.org/logo-og.png"]);
        Category::firstOrCreate(['category'=>"Angular",'image'=>"https://repository-images.githubusercontent.com/24195339/87018c00-694b-11e9-8b5f-c34826306d36"]);
        Category::firstOrCreate(['category'=>"C#",'image'=>"https://upload.wikimedia.org/wikipedia/commons/thumb/7/7a/C_Sharp_logo.svg/1200px-C_Sharp_logo.svg.png"]);
        Category::firstOrCreate(['category'=>"C++",'image'=>"https://upload.wikimedia.org/wikipedia/commons/1/18/ISO_C%2B%2B_Logo.svg"]);
        Category::firstOrCreate(['category'=>"Java",'image'=>"https://commons.bmstu.wiki/images/1/1c/Java12.jpeg"]);
        Category::firstOrCreate(['category'=>"algorithms",'image'=>"https://qph.fs.quoracdn.net/main-qimg-b1362c2db3d23c0fafa8623aa53fed75"]);
        Category::firstOrCreate(['category'=>"Laravel",'image'=>"https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Laravel.svg/1200px-Laravel.svg.png"]);
        Category::firstOrCreate(['category'=>"WordPress",'image'=>"https://pngimg.com/uploads/wordpress/wordpress_PNG51.png"]);
      
        echo 'categories created';
    }
}
