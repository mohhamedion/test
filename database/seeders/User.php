<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User as Users;
use Illuminate\Support\Facades\Hash;

class User extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Users::where('email', 'mohhamedion@gmail.com')->count() == 0) {

            Users::create([
                'email' => 'mohhamedion@gmail.com',
                'password' => Hash::make("test"),
                'firstname' => 'Mohamad',
                'lastname' => 'Alasali',
                'coin' => '5000',
                'role' => 'admin'
            ]);
            echo 'user created';
        } else {
            echo 'user exist';

        }


    }
}
