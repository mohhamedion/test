<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PaymentGate;

class PaymentGateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!PaymentGate::get()->count()) {

            $clientId = "AQrMsT4S3U65FCcpZqLxN1Zjz0bsM9YTs4oxqJpzEyq1tsXzJykv0ML-UJ8SFtPEVa_8FT7tVETS--Zt";
            $clientSecret = 'EMQAlVfxiRPNIN9m2U5hRmLjuKtSMjbV7Gu657327nxZNvxADn4yqiYkvY45xW0L8ZY6gGL2On1EqqtA';

            $production_clinet_id = 'AVoaBaM_F6YhDvPa-HMZoCyBT4Z-KKP98so-g7ioXKCHCSqD1eT9PkAfuDzx8yyqegDq2NNX3mxFYbEm';
            $production_secret = 'EFI_ek5LM5pQHyq3P6Bm6br2VD1xWUg6WEKKugoPTZL18ZE8svs_6au1yqHeTY4Rre59mnhzeoFa5Hh9';

            $data = [
                'sandbox' => [
                    'client_id' => $clientId,
                    'client_secret' => $clientSecret,
                ],
                'production' => [
                    'client_id' => $production_clinet_id,
                    'client_secret' => $production_secret,
                ]

            ];

            PaymentGate::create([
                'gate' => 'paypal',
                'data' => json_encode($data)
            ]);
            echo 'PaymentGate created';
        } else {
            echo 'PaymentGate exist';

        }
    }
}
