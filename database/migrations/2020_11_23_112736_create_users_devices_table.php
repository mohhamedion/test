<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_devices', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer("user_id")->nullable();
            $table->integer("test_id")->nullable();
            $table->boolean("android")->nullable();
            $table->boolean("androidPhone")->nullable();
            $table->boolean("dingding")->nullable();
            $table->boolean("iPhoneXR")->nullable();
            $table->boolean("iPhoneXSMax")->nullable();
            $table->boolean("ios")->nullable();
            $table->boolean("ipad")->nullable();
            $table->boolean("iphone")->nullable();
            $table->boolean("iphoneX")->nullable();
            $table->boolean("ipod")->nullable();
            $table->boolean("mobile")->nullable();
            $table->boolean("wechat")->nullable();
            $table->boolean("wechatMiniApp")->nullable();
            $table->boolean("windows")->nullable();
            $table->string("phone_type")->nullable();
      
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_devices');
    }
}
