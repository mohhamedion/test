<?php

use App\Http\Controllers\Api\AdController;
use App\Http\Controllers\Api\DepositController;
use App\Http\Controllers\Api\NotificationController;
use App\Http\Controllers\Api\PaypalController;
use App\Http\Controllers\Api\WalletController;
use App\Http\Controllers\Api\WithdrawalController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\TestController;
use App\Http\Controllers\Api\TestSessionController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\LevelController;
use App\Http\Controllers\Api\QuestionController;
use App\Http\Controllers\Api\CommentController;
use App\Http\Controllers\Api\RateTestController;
use App\Http\Controllers\Api\WebsiteMessageController;
use App\Http\Controllers\Api\SettingsController;

//admin
use App\Http\Controllers\Panel\UserController as UsersControllerPanel;
use App\Http\Controllers\Panel\TestController as TestControllerPanel;
use App\Http\Controllers\Panel\SessionController;
use App\Http\Controllers\Panel\DashboardController;
use App\Http\Controllers\Panel\MessagesController;
use App\Http\Controllers\Panel\WithdrawalRequestController;
use App\Http\Controllers\Panel\CategoryController as CategoryControllerPanel;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/profile/{user_id}', [UserController::class, 'show']);
Route::get('/settings', [SettingsController::class, 'index']);
Route::get('/result/{session_id}', [TestSessionController::class, 'getTestSessionResult']);


Route::post('/register', [UserController::class, 'register']);
Route::post('/login', [UserController::class, 'login']);
Route::post('/forgot-password', [UserController::class, 'forgotPassword']);
Route::post('/reset-password', [UserController::class, 'resetPassword']);

Route::post('/callus', [WebsiteMessageController::class, 'store']);

Route::group(['prefix' => 'ads'], function () {
    Route::get('/paginate', [AdController::class, 'paginate']);
});


Route::group(['prefix' => 'user'], function () {

    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('/wallet', [WalletController::class, 'showByUser']);
        Route::patch('/profile', [UserController::class, 'update']);
        Route::patch('/wallet', [WalletController::class, 'update']);
//        Route::get('/notifications',[UserController::class,'notifications']);
    });
});

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('/create-payment/', [PaypalController::class, 'createPayment']);
    Route::post('/execute-payment/', [PaypalController::class, 'executePayment']);
});

Route::group(['prefix' => 'notifications', 'middleware' => 'auth:api'], function () {
    Route::get('/', [NotificationController::class, 'paginate']);
    Route::get('/count-unread', [NotificationController::class, 'countUnread']);
});

Route::group(['prefix' => 'withdrawal', 'middleware' => 'auth:api'], function () {
    Route::get('/', [WithdrawalController::class, 'paginate']);
    Route::post('/', [WithdrawalController::class, 'sendWithdrawalRequest']);
});

Route::group(['prefix' => 'deposit', 'middleware' => 'auth:api'], function () {
    Route::get('/', [DepositController::class, 'paginate']);
});


Route::group(['prefix' => "/tests"], function () {

    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('/', [TestController::class, 'store']);
        Route::get('/getDraftTests', [TestController::class, 'getDraftTests']);
        Route::get('/getDraftTestsById/{test_id}', [TestController::class, 'getDraftTestsById']);
        Route::get('/getTestsById/{test_id}', [TestController::class, 'getTestsById']);
        Route::patch('/{test}', [TestController::class, 'update']);
        Route::patch('/publish/{test}', [TestController::class, 'publishTest']);
        Route::delete('/deleteDraftTest/{test_id}', [TestController::class, 'deleteDraftTest']);
    });

    Route::get('', [TestController::class, 'index']);
    Route::get('/bestfive', [TestController::class, 'getBestFive']);
    Route::get('/bestHomePage', [TestController::class, 'getBestByTestId']);
    Route::get('/{test_id}', [TestController::class, 'show']);

});


Route::group(['prefix' => "/testSessions", 'middleware' => 'auth:api'], function () {
    Route::get('', [TestSessionController::class, 'show']);
    Route::post('/', [TestSessionController::class, 'store']);
    Route::post('/answerQuestion', [TestSessionController::class, 'answerQuestion']);
    Route::get('/current-session-exist', [TestSessionController::class, 'currentSessionExist']);

});

Route::group(['prefix' => "/testSessions"], function () {
    Route::get('/paginate', [TestSessionController::class, 'paginate']);
    Route::get('/best-users/{test_id}', [TestSessionController::class, 'bestUsersForTestSession']);
    Route::get('/get-users-result/{session_id}', [TestSessionController::class, 'getLastSessionByTest']);

});


Route::group(['prefix' => "/rate", 'middleware' => 'auth:api'], function () {
    Route::post('/', [RateTestController::class, 'store']);
});


Route::group(['prefix' => "/questions", 'middleware' => 'auth:api'], function () {
    Route::post('/', [QuestionController::class, 'store']);
    Route::patch('/{question_id}', [QuestionController::class, 'update']);
    Route::delete('/delete/{question_id}', [QuestionController::class, 'deleteQuestion']);
});


Route::group(['prefix' => "/comments", 'middleware' => 'auth:api'], function () {
    Route::post('/', [CommentController::class, 'store']);
});


Route::group(['prefix' => "/categories"], function () {
    Route::get('/', [CategoryController::class, 'index']);
});

Route::group(['prefix' => "/levels"], function () {
    Route::get('/', [LevelController::class, 'index']);
});


Route::group(['prefix' => '/panel', 'middleware' => ['auth:api', 'admin']], function () {

    //dashbaord
    Route::get('/', [DashboardController::class, 'index'])->name('panel');

    Route::get('/settings', [DashboardController::class, 'settings'])->name('settings');

    Route::patch('/saveSettings', [DashboardController::class, 'saveSettings'])->name('saveSettings');

    Route::get('/users', [UsersControllerPanel::class, 'index'])->name('users');

    Route::get('/comments', [CommentController::class, 'index'])->name('comments');

    Route::get('/sessions', [SessionController::class, 'index'])->name('sessions');

    Route::get('/messages', [MessagesController::class, 'index'])->name('messages');

    Route::get('/tests', [TestControllerPanel::class, 'index'])->name('tests');
    Route::patch('/toggleApprove', [TestControllerPanel::class, 'toggleApprove'])->name('toggleApprove');


    Route::group(['prefix' => "withdrawalRequest", 'middleware' => 'auth'], function () {
        Route::get('/', [WithdrawalRequestController::class, 'index'])->name('withdrawalRequest.index');
        Route::post('/{withdrawal_id}', [WithdrawalRequestController::class, 'sendPayout'])->name('withdrawalRequest.accept');

    });

    Route::group(['prefix' => "ads"], function () {

        Route::get('/', [AdController::class, 'index'])->name('ads.index');
        Route::get('/create', [AdController::class, 'create'])->name('ads.create');
        Route::post('/', [AdController::class, 'store'])->name('ads.store');
        Route::delete('/{ad}', [AdController::class, 'destroy'])->name('ads.destroy');

    });

    Route::group(['prefix' => "categories"], function () {

        Route::get('/', [CategoryControllerPanel::class, 'index']);
        Route::post('/', [CategoryControllerPanel::class, 'store']);
        Route::patch('/{category}', [CategoryControllerPanel::class, 'update']);

    });

});
