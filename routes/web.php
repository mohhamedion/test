<?php

use App\Http\Controllers\Panel\AdController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Panel\DashboardController;
use App\Http\Controllers\Panel\UserController;
use App\Http\Controllers\Panel\CommentController;
use App\Http\Controllers\Panel\SessionController;
use App\Http\Controllers\Panel\MessagesController;
use App\Http\Controllers\Panel\TestController;
use \App\Http\Controllers\Panel\WithdrawalRequestController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/panel', function () {
//     return view('panel/users/index');
// });

// Route::get('/panel/login', function () {
//   return view('panel/auth/index');
// });
// Route::get('add/test',function(){
//   return view('index');
// });

// Route::get('/mail',function(){
//   $data['username'] = 'mohamad';
//   $data['test_id'] = '1';
//   $data['category'] = 'php';
//   Mail::to('mohhamedion@gmail.com')->send(new NewTest($data));
// });

// Route::get('/mailview',function(){


// // return view('mail.newtest')->with());

// });

//Route::get('/mail',function(){
//    $test = Test::first();
//    NotifyUsersToNewTest::dispatchNow($test);
//});


Route::get('/reset-password/{token}', function ($token) {
    return view('index');

})->middleware('guest')->name('password.reset');

Route::group(['prefix' => "panel"], (function () {

    Route::patch('/toggleApprove', [TestController::class, 'toggleApprove'])->name('toggleApprove')->middleware('auth');


    Route::get('/', [DashboardController::class, 'index'])->name('panel')->middleware('auth');
    Route::get('/users', [UserController::class, 'index'])->name('users')->middleware('auth');
    Route::get('/comments', [CommentController::class, 'index'])->name('comments')->middleware('auth');
    Route::get('/sessions', [SessionController::class, 'index'])->name('sessions')->middleware('auth');
    Route::get('/messages', [MessagesController::class, 'index'])->name('messages')->middleware('auth');
    Route::get('/tests', [TestController::class, 'index'])->name('tests')->middleware('auth');

    Route::get('/settings', [DashboardController::class, 'settings'])->name('settings')->middleware('auth');
    Route::patch('/saveSettings', [DashboardController::class, 'saveSettings'])->name('saveSettings')->middleware('auth');


    Route::group(['prefix' => "withdrawalRequest", 'middleware' => 'auth'], function () {
        Route::get('/', [WithdrawalRequestController::class, 'index'])->name('withdrawalRequest.index');
        Route::post('/{withdrawal_id}', [WithdrawalRequestController::class, 'sendPayout'])->name('withdrawalRequest.accept');

    });

    Route::group(['prefix' => "ads"], function () {

        Route::get('/', [AdController::class, 'index'])->name('ads.index')->middleware('auth');
        Route::get('/create', [AdController::class, 'create'])->name('ads.create')->middleware('auth');
        Route::post('/', [AdController::class, 'store'])->name('ads.store')->middleware('auth');
        Route::delete('/{ad}', [AdController::class, 'destroy'])->name('ads.destroy')->middleware('auth');

    });


    Route::get('/login', function () {
        return view('panel.auth.index');
    })->name('login.panel');

    Route::post('login', [UserController::class, 'login'])->name('login');
    Route::get('logout', [UserController::class, 'logout'])->name('logout');

}));


Route::get('/{any}', function () {
    return view('index');
})->where('any', '.*');

