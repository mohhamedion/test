<style type="text/css" media="all">
    body {
        direction: rtl;

    }

    #message {
        margin-top: 100px;
        font-size: 25px;
        color: black
    }

    /* #welcome{
        background:url('/background/background.svg'), #2b3137;

    } */
    #logo {
        color: white;
        margin-top: 40px
    }

    .buttonTry {
        margin-top: 10px
        align-items: center;
        background-color: #009c6b;
        color: #fff;
        cursor: pointer;
        text-overflow: ellipsis;
        text-decoration: none;
        white-space: nowrap;
        word-spacing: -3px;
        text-align: center;
        line-height: 1.2;
        width: 300px;
        min-width: 300px;
        max-width: 300px;
        font-weight: 300;
        font-size: 1.2em;
        border-radius: 2px;
        box-shadow: 0 4px 12px rgba(77, 186, 151, .5);
        padding: 1em 2.15em;
    }
</style>
<html>
<body>

<div id="welcome">
    <!-- <img id='logo' src="/logo/logo.png" alt=""> -->

    <div id="message">
        مرحبا {{$data['username']}}, يسعدنا اخبارك بوجود اختبار {{$data['category']}} جديد. هل انت مهتم؟
    </div>


    <br>
    <a href="quizzhunter.com/test/{{$data['test_id']}}" class="buttonTry">أختبر نفسك</a>
    <br>
    <img id='logo' src="/logo/logo.png" alt="">

    <p>اصدقائك من <a href="quizzhunter.com">quizzhunter.com</a></p>
</div>

</body>
</html>
