@extends('panel.layouts.app')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">settings</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">settings</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

  <div class='container'>
    <form action="{{route('saveSettings')}}" method='post'>
    @csrf
    @method('PATCH')

            <div class='form-group' >
                <label for="">Youtube video</label>

                <input name='youtube_home_video'
                       type="text" class='form-control'
                       value="{{$settings->youtube_home_video}}"
                >


            </div>
        <div class='form-group' >
            <label for="">payment settings</label>

            <input name='payment_status'
                   type="text" class='form-control'
                   value="{{$settings->payment_status}}"
            >



        </div>


            <div>
            <button class='btn btn-primary'>Save</button>
            </div>
    </form>



    </div>



</div>

@endsection
