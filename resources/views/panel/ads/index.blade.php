@extends('panel.layouts.app')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">DataTable with minimal features & hover style</h3>
                        </div>
                        <div class="card-bod ">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>title</th>
                                    <th>link</th>
                                    <th>image</th>
                                    <th>starts_at</th>
                                    <th>ends_at</th>
                                    <th>delete</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($table as $row)
                                    <tr>
                                        <td>{{$row->title}}</td>
                                        <td>{{$row->link}}</td>
                                        <td>{{$row->image}}</td>
                                        <td>{{$row->starts_at}}</td>
                                        <td>{{$row->ends_at}}</td>
                                        <td>
                                            <form action="{{route('ads.destroy',$row->id)}}" method="post">
                                                @method('delete')
                                                @csrf
                                                <button>delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach


                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>title</th>
                                    <th>link</th>
                                    <th>image</th>
                                    <th>starts_at</th>
                                    <th>ends_at</th>
                                    <th>delete</th>

                                </tr>
                                </tfoot>
                            </table>

                        </div>


                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->


                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>


        <!-- /.container-fluid -->
    </section>



@endsection
