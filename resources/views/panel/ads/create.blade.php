@extends('panel.layouts.app')

@section('content')


    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Messages</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">ads</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                            <div class="card-header">
                                <h3 class="card-title">ads</h3>
                            </div>
                            <!-- /.card-header -->
                            <form action="{{route('ads.store')}}" method="post" >
                                @method('POST')
                                @csrf
                                <div class="form-group">
                                    <label for="">title</label>
                                    <input type="text" name="title" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label for="">image link</label>
                                    <input type="text" name="image" class="form-control">
                                </div>


                                <div class="form-group">
                                    <label for="">link</label>
                                    <input type="text" name="link" class="form-control">
                                </div>


                                <div class="form-group">
                                    <label for="">position</label>
                                    <input type="number" name="position" class="form-control">
                                </div>

                                <button>create</button>

                            </form>
                            <!-- /.card-body -->


                        <!-- /.card -->


                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>



@endsection
