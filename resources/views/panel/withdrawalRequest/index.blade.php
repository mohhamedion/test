@extends('panel.layouts.app')

@section('content')


    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>DataTables</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Users</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">DataTable with minimal features & hover style</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>amount</th>
                                        <th>created_at</th>
                                        <th>user</th>
                                        <th>status</th>
                                        <th>accept</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($table as $row)

                                        <tr>
                                            <td>{{$row->id}}</td>
                                            <td>{{$row->amount}}</td>
                                            <td>{{$row->created_at}}</td>
                                            <td>{{$row->user_id}}</td>
                                            <td>{{$row->status}}</td>
                                                <td>
                                                    <form action="{{route('withdrawalRequest.accept',$row->id)}}" method="post">
                                                        @method('POST')
                                                        @csrf
                                                       <button>accept</button>
                                                    </form>
                                                </td>

                                        </tr>
                                    @endforeach


                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>id</th>
                                        <th>amount</th>
                                        <th>created_at</th>
                                        <th>user</th>
                                        <th>status</th>
                                        <th>accept</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->


                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>



@endsection
