require('./bootstrap');
window.Vue = require('vue');
import store from './store/index'
import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import axios from 'axios';
import App from './App.vue';
import Vue from "vue";
import router from './router/router';
import Testcard from './components/testcard/Testcard.vue';
import Header from './components/navigation/navigation.vue';
import Footer from './components/Footer.vue';
import Toast from "vue-toastification";
import "vue-toastification/dist/index.css";
import device from "vue-device-detector";
import LoadScript from 'vue-plugin-load-script';
import bootstrapVue from 'bootstrap-vue';
// import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

const options = {
    transition: "Vue-Toastification__bounce",
    maxToasts: 3,
    newestOnTop: true
};

Vue.use(bootstrapVue)
Vue.use(Toast, options);
Vue.use(VueAxios, axios);
Vue.use(VueRouter);
Vue.use(device);
Vue.use(LoadScript);

const notify = {
    install(Vue, options) {
        Vue.prototype.successHandler = (msg, title = '') => {

            Vue.$toast.info(msg, {
                position: "bottom-right",
                timeout: 3010,
                closeOnClick: true,
                pauseOnFocusLoss: true,
                pauseOnHover: true,
                draggable: true,
                draggablePercent: 0.31,
                showCloseButtonOnHover: false,
                hideProgressBar: true,
                closeButton: "button",
                icon: true,
                rtl: true
            });
        }
        Vue.prototype.handleError= (error) =>{
            let errors = typeof error === 'object' ? Object.values(error).join("\n") : error;
            Vue.$toast.error(errors, {
                position: "bottom-right",
                timeout: 3010,
                closeOnClick: true,
                pauseOnFocusLoss: true,
                pauseOnHover: true,
                draggable: true,
                draggablePercent: 0.31,
                showCloseButtonOnHover: false,
                hideProgressBar: true,
                closeButton: false,
                icon: true,
                rtl: true
            });
        }
    }
}
Vue.use(notify);
axios.interceptors.request.use(function (config) {
    config.headers.common = {
        Authorization: `Bearer ${localStorage.getItem("authToken")}`,
        "Content-Type": "application/json",
        Accept: "application/json"
    };

    return config;
});

Vue.component('navigator', Header);
Vue.component('footer-view', Footer);
Vue.component('test-card', Testcard);



const app = new Vue({

     ...App,
    router,
    el: '#app',
    store
});

