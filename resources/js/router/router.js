import VueRouter from "vue-router";
import HomeComponent from "../components/HomeComponent.vue";
import TestsComponents from "../components/TestsComponents.vue";
import TestPage from "../components/test/TestPage.vue";
import TestSession from "../components/test/TestSession.vue";
import ResultPage from "../components/result/resultPage.vue";
import Login from "../components/auth/Login.vue";
import Register from "../components/auth/Register.vue";
import notfound from "../components/notfound/404.vue";
import profileTestResult from '../components/user/testResults/testResults.vue';
import coin from '../components/coins/coin.vue';

const guest = (to, from, next) => {
    if (!localStorage.getItem("authToken")) {
        return next();
    } else {
        return next("/");
    }
};
const auth = (to, from, next) => {
    if (localStorage.getItem("authToken")) {
        return next();
    } else {
        return next("/login");
    }
};

const routes = [
    {
        name: "home",
        path: "/",
        component: HomeComponent
    },
    {
        name: "tests",
        path: "/tests",
        component: TestsComponents
    },
    {
        name: "test",
        path: "/test/:test_id",
        component: TestPage
    },
    {
        name: "session",
        path: "/session",
        beforeEnter: auth,
        component: TestSession
    },
    {
        name: "result",
        path: "/result/:session_id",
        component: ResultPage
    },
    {
        name: "addTest",
        path: "/add/test",
        beforeEnter: auth,
        component: () => import('../components/test/AddTest.vue')
    },
    {
        name: "addQuestion",
        path: "/add/question/:test_id",
        beforeEnter: auth,

        component: () => import('../components/questions/AddQuestion.vue')
    },
    {
        name: "login",
        path: "/login",
        beforeEnter: guest,
        component: Login
    },
    {
        name: "register",
        path: "/register",
        beforeEnter: guest,
        component: Register
    },

    {
        name: "reset-password",
        path: "/reset-password/:token",
        beforeEnter: guest,
        component: () => import('../components/reset-password/reset-password.vue')
    },
    {
        name: "forgot-password",
        path: "/forgot-password",
        beforeEnter: guest,
        component: () => import('../components/auth/forgot-password.vue')
    },
    {
        name: "profile",
        path: "/profile/:user_id/",
        redirect: "/profile/:user_id/",
        component: () => import('../components/user/Profile.vue'),
        children: [
            {
                name: "createdTests",
                path: "createdTests",
                component: () => import('../components/user/createdTests/createdTests.vue')
            },
            {
                name: "profileTestResult",
                path: "",
                component: profileTestResult
            }

        ]
    },

    {
        name: "settings",
        path: "/settings",
        component: () => import('../components/user/settings/settings.vue')
    },

    {
        name: "notfound",
        path: "*",
        component: notfound
    },

    {
        name: "coin",
        path: "/coin",
        component: coin
    },

    {
        name: "fortestcreators",
        path: "/for-test-creators",
        component: () => import('../components/fortesters/fortesters.vue')
    },
    {
        name: "policy",
        path: "/policy",
        component: () => import('../components/policy/policy.vue')
    },
    {
        name: "privacy",
        path: "/privacy",
        component: () => import('../components/privacy/privacy.vue')
    },
    {
        name: "callus",
        path: "/callus",
        component: () => import('../components/callus/callus.vue')
    },

    {
        name: "community",
        path: "/community",

        component: () => import('../components/community/community.vue')
    },

    {
        name: "wallet",
        path: "/wallet",
        beforeEnter: auth,
        component: () => import('../components/wallet/Wallet.vue')
    }
    ,
    {
        name: "purchases",
        path: "/purchases",
        beforeEnter: auth,
        component: () => import('../components/purchases/Purchases.vue')
    }
];

const router = new VueRouter({mode: "history", routes: routes});
export default router;
