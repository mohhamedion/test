import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

const store = {
    namespaced: true,
    state: {
        notificationsCount:null
    },
    getters: {
        getNotificationCount:(state)=>state.notificationsCount
    },
    actions: {
        getUnreadNotificationsCount({commit}){
            axios.get('/api/notifications/count-unread').then(res=>{
                commit('setNotificationsCount',res.data || null);
            });
        },
        increaseUnreadNotificationCount({commit}){
            commit('increaseNotificationsCount');
        },

        resetNotificationsCount({commit}){
            commit('resetNotificationsCount');
        }

    },
    mutations: {
        setNotificationsCount(state,notificationsCount){
            state.notificationsCount = notificationsCount;
        },
        resetNotificationsCount(state){
          state.notificationsCount=null;
        },
        increaseNotificationsCount(state){
            state.notificationsCount++;
        }
    }

}

export default store;
