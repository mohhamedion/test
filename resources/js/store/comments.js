import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

const store = {
    namespaced: true,
    state: {
        // comments:[]
    },
    getters: {
        // getcomments:(state)=>state.comments
    },
    actions: {
        createComment(test_id,comment) {
            axios
                .post("/api/comments", {
                    test_id ,
                    comment 
                })
                .then(response => {
                    if (response.status == 200) {
                        this.unshiftComment(response.data);
                        this.inputComment=null;
                    }
                });
        },
    },
    mutations: {
        // setcomments(state,comments){
        //     state.comments = comments;
        // }
}

}

export default store;
